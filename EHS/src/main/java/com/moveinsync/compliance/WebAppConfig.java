package com.moveinsync.compliance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.moveinsync.compliance.ets.persistence.config.PersistenceContext;

/**
 * This class is enabled with Spring MVC with @EnableWebMvc annotation. It also extends WebMvcConfigurerAdapter, which
 * provides empty methods that can be overridden to customize default configuration of Spring MVC.
 *
 * @author Amith Soman
 * @date Oct 17, 2014
 */

@Configuration
@EnableWebMvc
@EnableScheduling
@EnableJpaAuditing
//@ComponentScan(basePackages = { "com.moveinsync.compliance.custom", "com.moveinsync.compliance.ets.web.controller",
//	    "com.moveinsync.compliance.ets.services", "com.moveinsync.compliance.ets.model", "com.moveinsync.ets.infrastructure",
//	    "com.moveinsync.aws", "com.moveinsync.compliance.ets.services.*", "com.moveinsync.compliance.services, com.moveinsync.compliance.alerts.email" })
@Import(PersistenceContext.class)
public class WebAppConfig implements WebMvcConfigurer {

  static final Logger logger = LoggerFactory.getLogger(WebAppConfig.class);

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/rest/ehs/**").allowedOrigins("*");
  }

  @Bean
  public InternalResourceViewResolver viewResolver() {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("/WEB-INF/views/");
    resolver.setSuffix(".jsp");
    return resolver;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/css/**").addResourceLocations("/css/");
    registry.addResourceHandler("/javascript/**").addResourceLocations("/javascript/");
    registry.addResourceHandler("/images/**").addResourceLocations("/images/");
  }
}
