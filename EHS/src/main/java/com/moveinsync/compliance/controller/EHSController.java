package com.moveinsync.compliance.controller;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.moveinsync.compliance.dto.BusinessUnitCheckListDTO;
import com.moveinsync.compliance.dto.EHSAuditReportDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.dto.VehicleEHSDTO;
import com.moveinsync.compliance.ets.services.email.BounceManager;
import com.moveinsync.compliance.ets.services.email.EHSComplianceEntryEmailNotificationService;
import com.moveinsync.compliance.ets.services.executor.EHSFailedEmailNotifier;
import com.moveinsync.compliance.ets.web.util.WebConstants;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleAdjustmentVO;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleChecklistVO;
import com.moveinsync.compliance.ets.web.viewobject.BusinessUnitChecklistVO;
import com.moveinsync.compliance.ets.web.viewobject.EHSCheckListVO;
import com.moveinsync.compliance.models.VehicleEHSLastCheckStatusDTO;
import com.moveinsync.compliance.persistence.entities.EHSemailAlerts;
import com.moveinsync.compliance.persistence.repository.EHSAlertNotificationRepository;
import com.moveinsync.compliance.services.AdjustmentHandler;
import com.moveinsync.compliance.services.AdjustmentService;
import com.moveinsync.compliance.services.BusinessUnitCheckListService;
import com.moveinsync.compliance.services.ComplianceV2ServiceImpl;
import com.moveinsync.compliance.services.EHSChecklistService;
import com.moveinsync.compliance.services.EHSReportingService;
import com.moveinsync.compliance.services.VehicleEHSService;
import com.moveinsync.compliance.types.VehicleEHSStatus;
import com.moveinsync.compliance.utils.ComplianceUtils;

@CrossOrigin
@RestController
@RequestMapping("/rest/ehs")
public class EHSController {

  @Autowired
  VehicleEHSService vInfoService;

  @Autowired
  EHSChecklistService ehsChecklistService;

  @Autowired
  BusinessUnitCheckListService buCheckListService;

  @Autowired
  EHSReportingService ehsReportingService;

  @Autowired
  AdjustmentService adjustmentService;

  @Autowired
  EHSAlertNotificationRepository eHSAlertNotificationRepository;
  
  @Autowired
  EHSComplianceEntryEmailNotificationService eHSComplianceEntryEmailNotificationService;
  
  @Autowired ComplianceV2ServiceImpl compV2;

  static final Logger logger = LoggerFactory.getLogger(EHSController.class);

  @RequestMapping(method = RequestMethod.PUT, value = WebConstants.GENERATE_CHECKLIST)
  @ResponseBody
  public List<EHSCheckListVO> createCheckList(@RequestBody List<EHSCheckListVO> checkListVO) {
    List<EHSCheckListVO> checklistVOs = ehsChecklistService.ehsChecklistsGenerator(checkListVO);
    return checklistVOs;
  }

  @RequestMapping(method = RequestMethod.PUT, value = WebConstants.CHECKLIST_FOR_BUSINESS_UNIT)
  public ResponseEntity<?> createBUCheckList(@PathVariable(value = WebConstants.BUSINESS_UNIT_ID) String buId,
      @RequestBody List<BUVehicleChecklistVO> checkListVO) {
    List<BusinessUnitCheckListDTO> buCheckLists = buCheckListService.buChecklistEntryGenerator(buId, checkListVO);
    vInfoService.generateDefaultEntries(buId, buCheckLists);
    compV2.enableEhs(buId);
    return new ResponseEntity<Object>(HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.PUT, value = WebConstants.SAVE_PENALTY_CONFIGURATIONS)
  public ResponseEntity<?> addBUPenalty(@PathVariable(value = WebConstants.BUSINESS_UNIT_ID) String buId,
      @RequestBody List<BUVehicleAdjustmentVO> checkListVO) {
    buCheckListService.addPenaltyForBU(buId, checkListVO);
    return new ResponseEntity<Object>(HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.GET, value = WebConstants.VEHICLE_EHS_DETAILS)
  @ResponseBody
  private List<VehicleEHSDTO> getVehicleEHSInfo(@PathVariable(value = WebConstants.BUSINESS_UNIT_ID) String buId,
      @PathVariable(value = "vehicleId") String vehicleId) {
    List<VehicleEHSDTO> vInfos = vInfoService.getEHSByVehicleId(buId, vehicleId);
    return vInfos;
  }

  @RequestMapping(method = RequestMethod.GET, value = WebConstants.VEHICLES_FOR_BUSINESS_UNIT)
  @ResponseBody
  private List<VehicleEHSLastCheckStatusDTO> getVehicleList(
      @RequestParam(value = WebConstants.BUSINESS_UNIT_ID) String businessUnit) throws Exception {
    return compV2.getVehicles(businessUnit);
  }

  @RequestMapping(method = RequestMethod.GET, value = WebConstants.CHECKLIST_FOR_BUSINESS_UNIT)
  @ResponseBody
  private List<BusinessUnitChecklistVO> getCheckList(
      @PathVariable(value = WebConstants.BUSINESS_UNIT_ID) String businessUnit) {
    List<BusinessUnitChecklistVO> businessUnitChecklistVOs = buCheckListService.getChecklistVOForBU(businessUnit);
    Collections.sort(businessUnitChecklistVOs);
    return businessUnitChecklistVOs;
  }

  @RequestMapping(method = RequestMethod.POST, value = WebConstants.SAVE_VEHICLE_EHS_DETAILS)
  @ResponseBody
  private ResponseEntity<?> submitInfo(@RequestParam(value = WebConstants.BUSINESS_UNIT_ID) String businessUnit,
      @RequestBody VehicleEHSChecklistVO ehsStatusResponseVO, @PathVariable String vehicleId,
      @RequestHeader("username") String username, @RequestHeader("useremail") String userEmail,
      @RequestHeader("comment") String comment) {
    ExecutorService executor = Executors.newFixedThreadPool(2);
    EHSAuditReportDTO ehsReportAudit = ehsReportingService.processFailedEHSnotification(ehsStatusResponseVO,
        businessUnit, userEmail, comment);
//    ComplianceVehicle complianceVehicle = complianceService.getComplianceVehicle(businessUnit,
//        ehsStatusResponseVO.getRegistrationNumber());
    String ehsStatus = compV2.getEhsStatus(businessUnit, ehsStatusResponseVO.getRegistrationNumber());
    if (StringUtils.equals(ehsReportAudit.getOverallStatus(), VehicleEHSStatus.PASSED.name())
        && ehsStatus != null && ehsStatus.equals(VehicleEHSStatus.FAILED.name())) {
      logger.info("Sending instant allow email alert for businessUnitId : {} Registration number : {}",
          ehsStatusResponseVO.getBuid(), ehsStatusResponseVO.getRegistrationNumber());
      eHSComplianceEntryEmailNotificationService.sendAllowEntryEmailAlertNotification(ehsStatusResponseVO,
          ehsReportAudit);
      logger.info("EHS Allowed Entry Sent Successfully");
    }

    EHSemailAlerts eHSemailAlerts = eHSAlertNotificationRepository.findByBusinessUnitId(businessUnit);
    if (eHSemailAlerts != null && eHSemailAlerts.isFailedNotification()
        && StringUtils.equals(ehsReportAudit.getOverallStatus(), VehicleEHSStatus.FAILED.name())) {
      String[] validEmailIds = getEhsAlertEmailIds(eHSemailAlerts);
      if (ArrayUtils.isNotEmpty(validEmailIds)) {
        logger.info("Sending instant block email alert for buID : {} vehicle Registration number : {}",
            ehsStatusResponseVO.getBuid(), ehsStatusResponseVO.getRegistrationNumber());
        executor.execute(new EHSFailedEmailNotifier(ehsReportingService, ehsStatusResponseVO, businessUnit, userEmail,
            comment, validEmailIds,new String[] { "TransportComplianceTeam2@aexp.com" }, ehsReportAudit));
        logger.info("EHS Instant Email Sent Successfully");
      }
    }
    logger.debug("Adding adjustments to ETS !");
    executor.execute(new AdjustmentHandler(adjustmentService, businessUnit, ehsStatusResponseVO));
    executor.shutdown();
    ComplianceUtils.setAuditDetails(userEmail, comment);
    vInfoService.save(ehsStatusResponseVO, businessUnit);
    String status = ehsStatusResponseVO.getChecklistStatusMap().get("OVERALL");
    compV2.updateEhsStatus(businessUnit, vehicleId, ehsStatusResponseVO.getRegistrationNumber(), status);
    return new ResponseEntity<Object>(HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.GET, value = WebConstants.OVERALL_AUDIT_REPORT)
  @ResponseBody
  private List<EHSAuditReportDTO> getAuditReport(@PathVariable(value = WebConstants.BUSINESS_UNIT_ID) String buId,
      @RequestParam(value = WebConstants.VEHICLE_ID, required = false) String vehicleId,
      @RequestParam(value = "from") @DateTimeFormat(pattern = "dd-MM-yyyy") Date from,
      @RequestParam(value = "upto") @DateTimeFormat(pattern = "dd-MM-yyyy") Date upto) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(upto);
    calendar.add(Calendar.DAY_OF_YEAR, 1);
    if (vehicleId != null) {
      return ehsReportingService.getVehicleEHSAuditReport(buId, vehicleId, from, calendar.getTime());
    }
    return ehsReportingService.getEHSAuditReport(buId, from, calendar.getTime());
  }

  private String[] getEhsAlertEmailIds(EHSemailAlerts eHSemailAlerts) {
    List<String> emailIdList = Lists.newArrayList();
    if (StringUtils.isNotBlank(eHSemailAlerts.getEmailIDsSequrityTeam())) {
      emailIdList.addAll(Arrays.asList(eHSemailAlerts.getEmailIDsSequrityTeam().split(",")));
    }
    if (StringUtils.isNotBlank(eHSemailAlerts.getEmailIDsTransportTeam())) {
      emailIdList.addAll(Arrays.asList(eHSemailAlerts.getEmailIDsTransportTeam().split(",")));
    }
    if (CollectionUtils.isEmpty(emailIdList)) {
      return null;
    }
    String emails = StringUtils.join(emailIdList.toArray(), ',');
    String[] validMailIds = BounceManager.INSTANCE.getValidEmails(emails.split(","));
    return validMailIds;
  }

}