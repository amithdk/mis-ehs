package com.moveinsync.compliance.dto;

public class BusinessUnitCheckListDTO {

  private String buisinessUnit;

  private String checklistId;

  private boolean isMandatory;

  private int sequence;

  private String adjustment;

  public BusinessUnitCheckListDTO(String buisinessUnit, String checklistId) {
    this.buisinessUnit = buisinessUnit;
    this.checklistId = checklistId;
  }

  public String getBuisinessUnit() {
    return buisinessUnit;
  }

  public String getChecklistId() {
    return checklistId;
  }

  public boolean isMandatory() {
    return isMandatory;
  }

  public int getSequence() {
    return sequence;
  }

  public String getAdjustment() {
    return adjustment;
  }

  public void setAdjustment(String adjustment) {
    this.adjustment = adjustment;
  }

  public void setMandatory(boolean isMandatory) {
    this.isMandatory = isMandatory;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

}
