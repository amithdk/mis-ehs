package com.moveinsync.compliance.dto;

import java.util.Date;

public class EHSAdditionalLogDTO {

  private String businessUnit;

  private String vehicleId;

  private String facility;

  private String driverName;

  private String kmsTravelled;

  private Date updateTime;

  public String getBusinessUnit() {
    return businessUnit;
  }

  public void setBusinessUnit(String businessUnit) {
    this.businessUnit = businessUnit;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public String getFacility() {
    return facility;
  }

  public void setFacility(String facility) {
    this.facility = facility;
  }

  public String getDriverName() {
    return driverName;
  }

  public void setDriverName(String driverName) {
    this.driverName = driverName;
  }

  public String getKmsTravelled() {
    return kmsTravelled;
  }

  public void setKmsTravelled(String kmsTravelled) {
    this.kmsTravelled = kmsTravelled;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
