package com.moveinsync.compliance.dto;

import java.util.Date;
import java.util.List;

public class EHSAuditReportDTO {

  private String changeTime;

  private String vendorName;

  private String vehicleRegistration;

  private String auditBy;

  private String overallStatus;

  private List<String> fieldsRejected;

  private String comment;

  private String cabType;

  private String office;
  
  private String driver;
  
  private String kmsTravelled;

  private Date ehsExpiryDate;

  public String getChangeTime() {
    return changeTime;
  }

  public String getVendorName() {
    return vendorName;
  }

  public String getVehicleRegistration() {
    return vehicleRegistration;
  }

  public String getAuditBy() {
    return auditBy;
  }

  public String getOverallStatus() {
    return overallStatus;
  }

  public List<String> getFieldsRejected() {
    return fieldsRejected;
  }

  public String getComment() {
    return comment;
  }

  public void setChangeTime(String changeTime) {
    this.changeTime = changeTime;
  }

  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  public void setVehicleRegistration(String vehicleRegistration) {
    this.vehicleRegistration = vehicleRegistration;
  }

  public void setAuditBy(String auditBy) {
    this.auditBy = auditBy;
  }

  public void setOverallStatus(String overallStatus) {
    this.overallStatus = overallStatus;
  }

  public void setFieldsRejected(List<String> fieldsRejected) {
    this.fieldsRejected = fieldsRejected;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getCabType() {
    return cabType;
  }

  public void setCabType(String cabType) {
    this.cabType = cabType;
  }

  public String getOffice() {
    return office;
  }

  public void setOffice(String office) {
    this.office = office;
  }

  public Date getEhsExpiryDate() {
    return ehsExpiryDate;
  }

  public void setEhsExpiryDate(Date ehsExpiryDate) {
    this.ehsExpiryDate = ehsExpiryDate;
  }

  public String getDriver() {
    return driver;
  }

  public void setDriver(String driver) {
    this.driver = driver;
  }

  public String getKmsTravelled() {
    return kmsTravelled;
  }

  public void setKmsTravelled(String kmsTravelled) {
    this.kmsTravelled = kmsTravelled;
  }

@Override
public String toString() {
	return "EHSAuditReportDTO [changeTime=" + changeTime + ", vendorName=" + vendorName + ", vehicleRegistration="
			+ vehicleRegistration + ", auditBy=" + auditBy + ", overallStatus=" + overallStatus + ", fieldsRejected="
			+ fieldsRejected + ", comment=" + comment + ", cabType=" + cabType + ", office=" + office + ", driver="
			+ driver + ", kmsTravelled=" + kmsTravelled + ", ehsExpiryDate=" + ehsExpiryDate + "]";
}
  
  

}
