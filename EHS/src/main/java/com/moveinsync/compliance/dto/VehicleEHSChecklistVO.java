package com.moveinsync.compliance.dto;

import java.util.Map;

public class VehicleEHSChecklistVO {

  private String buid;

  private String vehicleId;

  private String registrationNumber;

  /* contains cheklistId and status map */
  private Map<String, String> checklistStatusMap;

  private String comment;

  private String facility;

  private String driverName;

  private String kmsTravelled;

  public String getBuid() {
    return buid;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public String getComment() {
    return comment;
  }

  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public String getFacility() {
    return facility;
  }

  public String getDriverName() {
    return driverName;
  }

  public String getKmsTravelled() {
    return kmsTravelled;
  }

  public Map<String, String> getChecklistStatusMap() {
    return checklistStatusMap;
  }

}
