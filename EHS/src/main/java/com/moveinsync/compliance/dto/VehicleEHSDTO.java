package com.moveinsync.compliance.dto;

import com.moveinsync.compliance.ets.model.EHSNotificationStatus;
import com.moveinsync.compliance.types.VehicleEHSStatus;

import java.util.Date;

public class VehicleEHSDTO {

  private String buid;

  private String vehicleId;

  private String checklistId;

  private VehicleEHSStatus ehsStatus;

  private Date ehsStatusUpdateTime;

  private Date ehsExpiryTime;

  private EHSNotificationStatus notificationStatus;
  
  private String complianceStatus;

  private String comment;

  public String getBuid() {
    return buid;
  }

  public void setBuid(String buid) {
    this.buid = buid;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public String getChecklistId() {
    return checklistId;
  }

  public void setChecklistId(String checklistId) {
    this.checklistId = checklistId;
  }

  public VehicleEHSStatus getEhsStatus() {
    return ehsStatus;
  }

  public void setEhsStatus(VehicleEHSStatus ehsStatus) {
    this.ehsStatus = ehsStatus;
  }

  public Date getEhsStatusUpdateTime() {
    return ehsStatusUpdateTime;
  }

  public void setEhsStatusUpdateTime(Date ehsStatusUpdateTime) {
    this.ehsStatusUpdateTime = ehsStatusUpdateTime;
  }

  public Date getEhsExpiryTime() {
    return ehsExpiryTime;
  }

  public void setEhsExpiryTime(Date ehsExpiryTime) {
    this.ehsExpiryTime = ehsExpiryTime;
  }

  public EHSNotificationStatus getNotificationStatus() {
    return notificationStatus;
  }

  public void setNotificationStatus(EHSNotificationStatus notificationStatus) {
    this.notificationStatus = notificationStatus;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getComplianceStatus() {
    return complianceStatus;
  }

  public void setComplianceStatus(String complianceStatus) {
    this.complianceStatus = complianceStatus;
  }

}
