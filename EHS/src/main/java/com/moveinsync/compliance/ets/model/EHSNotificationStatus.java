/**
 *
 */
package com.moveinsync.compliance.ets.model;

/**
 * @author kapoor
 *
 */
public enum EHSNotificationStatus {
    PROCEDDED, NOT_PROCESSED
}
