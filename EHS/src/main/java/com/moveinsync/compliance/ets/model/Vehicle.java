package com.moveinsync.compliance.ets.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Amith Soman
 * @Oct 31, 2014
 */
public class Vehicle implements Serializable, Cloneable {

	private static final long serialVersionUID = -7567249738218712244L;

	private String vehicleId;
	private String etsVehicleId;
	private String registartionNumber;
	private String vehicleType;
	private String vendor;
	private String status;
	private String compliance;
	private Date activationDate;
	private String[] vendorMailds;
	private String vendorId;
	private String vehicleTypeId;
	private Date createdTime;
	private Date updatedTime;
	private Date statusCreatedTime;
	private Date statusUpdatedTime;
	private int nativeVehicleId;
	private String fuelType;
	private String businessUnitID;

	public Vehicle() {
	}

	public Vehicle(String vehicleId, String registartionNumber, String vehicleType, String vendor) {
		super();
		this.vehicleId = vehicleId;
		this.registartionNumber = registartionNumber;
		this.vehicleType = vehicleType;
		this.vendor = vendor;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getRegistartionNumber() {
		return registartionNumber;
	}

	public void setRegistartionNumber(String registartionNumber) {
		this.registartionNumber = registartionNumber;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCompliance() {
		return compliance;
	}

	public void setCompliance(String compliance) {
		this.compliance = compliance;
	}

	public String getEtsVehicleId() {
		return etsVehicleId;
	}

	public void setEtsVehicleId(String etsVehicleId) {
		this.etsVehicleId = etsVehicleId;
	}

	@Override
	public String toString() {
		return "Vehicle [etsVehicleId=" + etsVehicleId + ", registartionNumber=" + registartionNumber + ", status="
				+ status + "]";
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public String[] getVendorMailds() {
		return vendorMailds;
	}

	public void setVendorMailds(String[] vendorMailds) {
		this.vendorMailds = vendorMailds;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

    public String getVehicleTypeId() {
      return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
      this.vehicleTypeId = vehicleTypeId;
    }

    public Date getCreatedTime() {
      return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
      this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
      return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
      this.updatedTime = updatedTime;
    }
    
	public Date getStatusCreatedTime() {
		return statusCreatedTime;
	}

	public void setStatusCreatedTime(Date statusCreatedTime) {
		this.statusCreatedTime = statusCreatedTime;
	}

	public Date getStatusUpdatedTime() {
		return statusUpdatedTime;
	}

	public void setStatusUpdatedTime(Date statusUpdatedTime) {
		this.statusUpdatedTime = statusUpdatedTime;
	}

    public int getNativeVehicleId() {
      return nativeVehicleId;
    }

    public void setNativeVehicleId(int nativeVehicleId) {
      this.nativeVehicleId = nativeVehicleId;
    }

    public String getFuelType() {
      return fuelType;
    }

    public void setFuelType(String fuelType) {
      this.fuelType = fuelType;
    }

	public String getBusinessUnitID() {
		return businessUnitID;
	}

	public void setBusinessUnitID(String businessUnitID) {
		this.businessUnitID = businessUnitID;
	}
}
