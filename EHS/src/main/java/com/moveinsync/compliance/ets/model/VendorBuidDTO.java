package com.moveinsync.compliance.ets.model;

import java.util.List;
import java.util.Map;

public class VendorBuidDTO {
	
	private Integer offset;
	
	private Integer limit;
	
	private Map<String, List<String>> buidVendorMap;
	
	private String searchData;

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Map<String, List<String>> getBuidVendorMap() {
		return buidVendorMap;
	}

	public void setBuidVendorMap(Map<String, List<String>> buidVendorMap) {
		this.buidVendorMap = buidVendorMap;
	}

	public String getSearchData() {
		return searchData;
	}

	public void setSearchData(String searchData) {
		this.searchData = searchData;
	}

	
	

}
