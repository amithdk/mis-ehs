package com.moveinsync.compliance.ets.model;

public class VmsDomainUrls {
	String domain;
	String urlVehicle;
	String urlBuVehicles;

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUrlVehicle() {
		return urlVehicle;
	}

	public void setUrlVehicle(String urlVehicle) {
		this.urlVehicle = urlVehicle;
	}

	public String getUrlBuVehicles() {
		return urlBuVehicles;
	}

	public void setUrlBuVehicles(String urlBuVehicles) {
		this.urlBuVehicles = urlBuVehicles;
	}
}
