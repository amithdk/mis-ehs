package com.moveinsync.compliance.ets.persistence.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.moveinsync.comm.clients.MulticastClient;
import com.moveinsync.compliance.ets.model.VmsDomainUrls;

@Configuration
public class GeneralConfiguration {

	@Autowired
	Environment env;

	@Bean
	public VmsDomainUrls getDomainUrl() {
		VmsDomainUrls url = new VmsDomainUrls();
		url.setDomain(env.getProperty("vms.domain"));
		url.setUrlVehicle(env.getProperty("vms.vehicle"));
		url.setUrlBuVehicles(env.getProperty("vms.vehicles"));
		return url;
	}

	@Bean
	public MulticastClient getMultiCastClient() {
		MulticastClient multiCastClient = new MulticastClient(env.getProperty("communication.domain"));
		return multiCastClient;
	}

	@Bean
	public RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
		return restTemplate;
	}
}
