package com.moveinsync.compliance.ets.persistence.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.moveinsync.data.envers.repository.support.MISRevisionRepositoryFactoryBean;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author Amith Soman
 * @date Oct 15, 2014
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.moveinsync.compliance.ets.persistence.repository",
		"com.moveinsync.compliance.persistence.repository" }, 
	repositoryFactoryBeanClass = MISRevisionRepositoryFactoryBean.class)
public class PersistenceContext {

  @Autowired
  private Environment env;

  @Bean
  public DataSource dataSource() {
    HikariConfig hikariConfig = new HikariConfig();
    hikariConfig.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
    hikariConfig.setJdbcUrl(env.getRequiredProperty("jdbc.url"));
    hikariConfig.setUsername(env.getProperty("jdbc.username"));
    hikariConfig.setPassword(env.getProperty("jdbc.password"));

    hikariConfig.setMaximumPoolSize(Integer.parseInt(env.getProperty("dataSource.maxPoolConnection")));
    hikariConfig.setMinimumIdle(Integer.parseInt(env.getProperty("dataSource.minIdleConnection")));
    
    hikariConfig.setConnectionTestQuery("SELECT 1");
    hikariConfig.setPoolName("compliance_pool");

    hikariConfig.addDataSourceProperty("dataSource.defaultRowFetchSize",env.getProperty("dataSource.defaultRowFetchSize"));
    HikariDataSource dataSource = new HikariDataSource(hikariConfig);

    return dataSource;
}

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactoryBean.setDataSource(dataSource());
    entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    entityManagerFactoryBean.setPackagesToScan("com.moveinsync.compliance.ets.persistence.entities","com.moveinsync.compliance.persistence.entities",
        "com.moveinsync.data.envers.models", "com.mis.compliance.entities");
    entityManagerFactoryBean.setJpaProperties(hibernateProperties());
    return entityManagerFactoryBean;
  }

  @Bean
  public JpaTransactionManager transactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
    return transactionManager;
  }

  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  private Properties hibernateProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
    properties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));
    properties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
    properties.put("hibernate.jdbc.lob.non_contextual_creation",
        env.getRequiredProperty("hibernate.jdbc.lob.non_contextual_creation"));
    properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("hibernate.enable_lazy_load_no_trans"));
    return properties;
  }

}
