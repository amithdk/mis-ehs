package com.moveinsync.compliance.ets.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Ramesh
 * 23 Feb 2018
 */

@Entity
@Table(name = "revinfo")
public class RevInfo {

  @Id
  @Column(name = "rev")
  private Integer rev;

  @Column(name = "revtstmp")
  private long timstamp;

  @Column(name = "mis_user")
  private String misUser;

  @Column(name = "comment")
  private String comment;

  public Integer getRev() {
    return rev;
  }

  public void setRev(Integer rev) {
    this.rev = rev;
  }

  public long getTimstamp() {
    return timstamp;
  }

  public void setTimstamp(long timstamp) {
    this.timstamp = timstamp;
  }

  public String getMisUser() {
    return misUser;
  }

  public void setMisUser(String misUser) {
    this.misUser = misUser;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

}
