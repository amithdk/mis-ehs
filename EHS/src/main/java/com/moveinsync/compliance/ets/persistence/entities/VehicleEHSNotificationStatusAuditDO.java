package com.moveinsync.compliance.ets.persistence.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_ehs_notification_status_aud")
public class VehicleEHSNotificationStatusAuditDO {

  @EmbeddedId
  VehicleEHSNotificationStatusAuditId ehsAuditId;

  @Column(name = "ehs_status")
  private String ehsStatus;

  @Column(name = "ehs_expiry_time")
  private Date ehsExpiryTime;

  @Column(name = "notification_status")
  private String notificationStatus;

  @Column(name = "comment")
  private String comment;

  @Column(name = "rev")
  private Integer rev;
  
  @Column(name = "ehs_audited")
  private boolean ehsAudited;

  @OneToOne(cascade = { CascadeType.ALL })
  @JoinColumn(name = "rev", insertable = false, updatable = false)
  private RevInfo revinfo;

  public VehicleEHSNotificationStatusAuditDO() {
  }

  public VehicleEHSNotificationStatusAuditDO(String vehicleId, String businessUnitId, Date updateTime) {
    this.ehsAuditId = new VehicleEHSNotificationStatusAuditId(vehicleId, businessUnitId, updateTime);
  }

  public VehicleEHSNotificationStatusAuditId getEhsAuditId() {
    return ehsAuditId;
  }

  public String getEhsStatus() {
    return ehsStatus;
  }

  public void setEhsStatus(String ehsStatus) {
    this.ehsStatus = ehsStatus;
  }

  public Date getEhsExpiryTime() {
    return ehsExpiryTime;
  }

  public void setEhsExpiryTime(Date ehsExpiryTime) {
    this.ehsExpiryTime = ehsExpiryTime;
  }

  public String getNotificationStatus() {
    return notificationStatus;
  }

  public void setNotificationStatus(String notificationStatus) {
    this.notificationStatus = notificationStatus;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Integer getRev() {
    return rev;
  }

  public RevInfo getRevinfo() {
    return revinfo;
  }

  public void setRevinfo(RevInfo revinfo) {
    this.revinfo = revinfo;
  }

  public boolean isEhsAudited() {
    return ehsAudited;
  }

  public void setEhsAudited(boolean ehsAudited) {
    this.ehsAudited = ehsAudited;
  }

  
}
