package com.moveinsync.compliance.ets.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VehicleEHSNotificationStatusAuditId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 590364757894040651L;

  @Column(name = "vehicle_id")
  private String vehicleId;

  @Column(name = "ehs_status_update_time")
  private Date ehsStatusUpdateTime;

  @Column(name = "business_unit_id")
  private String businessUnitId;

  public VehicleEHSNotificationStatusAuditId() {
  }

  public VehicleEHSNotificationStatusAuditId(String vehicleId, String businessUnitId, Date updateTime) {
    this.vehicleId = vehicleId;
    this.businessUnitId = businessUnitId;
    this.ehsStatusUpdateTime = updateTime;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public String getBusinessUnitId() {
    return businessUnitId;
  }

  public Date getEhsStatusUpdateTime() {
    return ehsStatusUpdateTime;
  }

}
