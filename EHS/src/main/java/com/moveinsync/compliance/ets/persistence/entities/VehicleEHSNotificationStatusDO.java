/**
 *
 */
package com.moveinsync.compliance.ets.persistence.entities;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.envers.Audited;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author kapoor
 *
 */
@Audited
@Entity
@Table(name = "vehicle_ehs_notification_status")
@DynamicUpdate
@SelectBeforeUpdate
@IdClass(VehicleEHSNotificationStatusDOIdClass.class)
public class VehicleEHSNotificationStatusDO implements Serializable {

  private static final long serialVersionUID = -3230570070988058080L;

  @Id
  @Column(name = "vehicle_id")
  private String vehicleId;

  @Id
  @Column(name = "business_unit_id")
  private String businessUnitId;

  @Column(name = "ehs_status")
  private String ehsStatus;

  @Column(name = "ehs_status_update_time")
  private Date ehsStatusUpdateTime;

  @Column(name = "ehs_expiry_time")
  private Date ehsExpiryTime;

  @Column(name = "notification_status")
  private String notificationStatus;

  @Column(name = "comment")
  private String comment;
  
  @Column(name = "ehs_audited")
  private boolean ehsAudited;

  VehicleEHSNotificationStatusDO() {
  }

  public VehicleEHSNotificationStatusDO(String vehicleId, String businessUnitId) {
    this.vehicleId = vehicleId;
    this.businessUnitId = businessUnitId;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public String getBusinessUnitId() {
    return businessUnitId;
  }

  public String getEhsStatus() {
    return ehsStatus;
  }

  public Date getEhsStatusUpdateTime() {
    return ehsStatusUpdateTime;
  }

  public Date getEhsExpiryTime() {
    return ehsExpiryTime;
  }

  public String getNotificationStatus() {
    return notificationStatus;
  }

  public void setEhsStatus(String ehsStatus) {
    this.ehsStatus = ehsStatus;
  }

  public void setEhsStatusUpdateTime(Date ehsStatusUpdateTime) {
    this.ehsStatusUpdateTime = ehsStatusUpdateTime;
  }

  public void setEhsExpiryTime(Date ehsExpiryTime) {
    this.ehsExpiryTime = ehsExpiryTime;
  }

  public void setNotificationStatus(String notificationStatus) {
    this.notificationStatus = notificationStatus;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public boolean isEhsAudited() {
    return ehsAudited;
  }

  public void setEhsAudited(boolean ehsAudited) {
    this.ehsAudited = ehsAudited;
  }
}
