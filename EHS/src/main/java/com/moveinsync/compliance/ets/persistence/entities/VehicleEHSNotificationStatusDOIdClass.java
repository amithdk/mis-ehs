/**
 * 
 */
package com.moveinsync.compliance.ets.persistence.entities;

import java.io.Serializable;

/**
 * @author kapoor
 *
 */
public class VehicleEHSNotificationStatusDOIdClass implements Serializable {

  private static final long serialVersionUID = 6432853163181184573L;

  private String vehicleId;

  private String businessUnitId;

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public String getBusinessUnitId() {
    return businessUnitId;
  }

  public void setBusinessUnitId(String businessUnitId) {
    this.businessUnitId = businessUnitId;
  }

}
