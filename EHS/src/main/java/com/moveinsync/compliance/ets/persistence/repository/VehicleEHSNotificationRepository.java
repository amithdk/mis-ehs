/**
 *
 */
package com.moveinsync.compliance.ets.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.moveinsync.compliance.ets.persistence.entities.VehicleEHSNotificationStatusDO;

import java.util.Date;
import java.util.List;

/**
 * @author kapoor
 *
 */
public interface VehicleEHSNotificationRepository extends JpaRepository<VehicleEHSNotificationStatusDO, String> {

    String QUERY_FIND_VEHICLE_STATUS_BY_ID_AND_BU = "from VehicleEHSNotificationStatusDO st where st.vehicleId=:vehicleId AND st.businessUnitId=:businessUnitId";
    String QUERY_FIND_VEHICLES_STATUS_BY_BU_AND_TIME = "from VehicleEHSNotificationStatusDO where businessUnitId=?1 AND ehsExpiryTime >=?2 AND ehsExpiryTime <=?3 AND ehsStatus =?4 AND notificationStatus =?5";
    String QUERY_FIND_VEHICLES_STATUS_BY_BU_AND_TIME_FOR_EXPIRY = "from VehicleEHSNotificationStatusDO where businessUnitId=?1 AND ehsExpiryTime >=?2 AND ehsExpiryTime <=?3 AND ehsStatus =?4";
    String QUERY_FIND_VEHICLES_STATUS_BY_BU = "from VehicleEHSNotificationStatusDO where businessUnitId=?1 AND ehsStatus=?2 AND notificationStatus=?3";
    String QUERY_FIND_VEHICLES_STATUS_BY_EHS_STATUS = "from VehicleEHSNotificationStatusDO where businessUnitId = ?1 and ehsStatus = ?2";

    @Query(QUERY_FIND_VEHICLE_STATUS_BY_ID_AND_BU)
    VehicleEHSNotificationStatusDO findByBusinessUnitAndVehicleId(@Param("vehicleId") String vehicleId,
            @Param("businessUnitId") String businessUnitId);

    @Query(QUERY_FIND_VEHICLES_STATUS_BY_BU_AND_TIME)
    List<VehicleEHSNotificationStatusDO> findByBUAndTimeRange(String businessUnitId, Date startTime, Date endTime,
            String ehsstatus, String status);

    @Query(QUERY_FIND_VEHICLES_STATUS_BY_BU_AND_TIME_FOR_EXPIRY)
    List<VehicleEHSNotificationStatusDO> findByBUAndTimeRangeForExpiry(String businessUnitId, Date startTime, Date endTime,
            String ehsstatus);

    @Query(QUERY_FIND_VEHICLES_STATUS_BY_BU)
    List<VehicleEHSNotificationStatusDO> findByBU(String businessUnit, String ehsStatus, String notificationStatus);
    
    @Query(QUERY_FIND_VEHICLES_STATUS_BY_EHS_STATUS)
    List<VehicleEHSNotificationStatusDO> findNotificationStatusByStatus(String buid,String ehsStatus);
}
