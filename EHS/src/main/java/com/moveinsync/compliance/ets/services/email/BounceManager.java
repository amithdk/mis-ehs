package com.moveinsync.compliance.ets.services.email;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.moveinsync.compliance.ets.web.util.WebConstants;

import java.net.URI;

public enum BounceManager {

  INSTANCE;

  public String[] getValidEmails(String[] emailList) {
    RestTemplate restTemplate = new RestTemplate();

    StringBuilder sb = new StringBuilder(WebConstants.EMAIL_BOUNCE_SERVER_URI);
    sb.append("filter/");

    String emails = StringUtils.join(emailList, ',');
    URI targetURL = UriComponentsBuilder.fromUriString(sb.toString()).queryParam("emailList", emails).build().toUri();

    try {
      return restTemplate.getForObject(targetURL, String[].class);
    } catch (Exception ex) {
      return emailList;
    }
  }
}

