package com.moveinsync.compliance.ets.services.email;

import com.google.gson.JsonSyntaxException;
import com.moveinsync.comm.clients.MulticastClient;
import com.moveinsync.comm.dto.M_Multicast;
import com.moveinsync.comm.messagepayload.M_MessagePayload;
import com.moveinsync.comm.recipient.M_EmailId;
import com.moveinsync.comm.recipient.M_Recipient;
import com.moveinsync.comm.status.CommunicationChannel;
import com.moveinsync.comm.utils.M_Sender;
import com.moveinsync.compliance.ets.web.util.WebConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class CommunicationService {
  static final Logger logger = LoggerFactory.getLogger(CommunicationService.class);

  
   @Autowired
   MulticastClient multicastClient;

  public void sendEmailUsingCommunicationChannnel(String[] emailIds, String subject, String mailBody,
      String businessUnitId) {
    Map<CommunicationChannel, String> senderMap = new HashMap<CommunicationChannel, String>();
    senderMap.put(CommunicationChannel.EMAIL, WebConstants.SENDER_EMAIL);
    M_Sender sender = new M_Sender(senderMap);

    List<M_Recipient> recipientList = getEmailRecipentList(emailIds);
    M_MessagePayload payload = M_MessagePayload.builder().addEmailPayload(subject, mailBody, true).build();
    M_Multicast multicastModel = M_Multicast
        .builder(businessUnitId, recipientList, payload, WebConstants.COMMUNICIATION_EVENT_NAME).addSenderId(sender)
        .build();

    try {
      multicastClient.send(multicastModel);
    } catch (JsonSyntaxException | IOException e) {
      logger.error(e.toString());
    }
  }

  public List<M_Recipient> getEmailRecipentList(String[] emailIds) {
    List<M_Recipient> recipientList = new ArrayList<M_Recipient>();
    for (String emailId : emailIds) {
      M_Recipient recipient = M_Recipient.builder(UUID.randomUUID().toString())
          .addEmailId(new M_EmailId(M_EmailId.EmailRecipientType.TO, emailId)).build();
      recipientList.add(recipient);
    }
    return recipientList;
  }
  
  public void sendEmailUsingCommunicationChannnelForVehicleServices(String[] tranportTeamemailIds,String[] vendorTeamemailIds, String subject, String mailBody,
	      String businessUnitId) {
	    Map<CommunicationChannel, String> senderMap = new HashMap<CommunicationChannel, String>();
	    senderMap.put(CommunicationChannel.EMAIL, WebConstants.SENDER_EMAIL);
	    M_Sender sender = new M_Sender(senderMap);

	    List<M_Recipient> recipientList = getEmailRecipentListForVehicleServices(tranportTeamemailIds,vendorTeamemailIds);
	    M_MessagePayload payload = M_MessagePayload.builder().addEmailPayload(subject, mailBody, true).build();
	    M_Multicast multicastModel = M_Multicast
	        .builder(businessUnitId, recipientList, payload, WebConstants.COMMUNICIATION_EVENT_NAME).addSenderId(sender)
	        .build();

	    try {
	      multicastClient.send(multicastModel);
	    } catch (JsonSyntaxException | IOException e) {
	      logger.error(e.toString());
	    }
	  }
  
	private List<M_Recipient> getEmailRecipentListForVehicleServices(String[] transportTeamemailIds, String[] vendorTeamemailIds) {
		List<M_Recipient> recipientList = new ArrayList<M_Recipient>();
		for (String emailId : transportTeamemailIds) {
			M_Recipient recipientCC = M_Recipient.builder(UUID.randomUUID().toString())
					.addEmailId(new M_EmailId(M_EmailId.EmailRecipientType.CC, emailId)).build();
			recipientList.add(recipientCC);
		}
		for (String emailId : vendorTeamemailIds) {
			M_Recipient recipientCC = M_Recipient.builder(UUID.randomUUID().toString())
					.addEmailId(new M_EmailId(M_EmailId.EmailRecipientType.TO, emailId)).build();
			recipientList.add(recipientCC);
		}
		return recipientList;
	}
}
