package com.moveinsync.compliance.ets.services.email;

import com.google.gson.JsonSyntaxException;
import com.moveinsync.comm.clients.MulticastClient;
import com.moveinsync.comm.dto.M_Multicast;
import com.moveinsync.comm.messagepayload.M_MessagePayload;
import com.moveinsync.comm.recipient.M_EmailId;
import com.moveinsync.comm.recipient.M_Recipient;
import com.moveinsync.comm.status.CommunicationChannel;
import com.moveinsync.comm.utils.M_Sender;
import com.moveinsync.compliance.ets.web.util.WebConstants;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class EHSCommunicationService {

	static final Logger logger = LoggerFactory.getLogger(EHSCommunicationService.class);
	
	@Autowired
	MulticastClient multicastClient;

	/**
	 * @param vendorEmailId  : vendor email ID
	 * @param emailIds       : transport and security team email ID
	 * @param mailSubject    : subject line of e-mail
	 * @param mailBody       : data need to be sent in body of e-mail
	 * @param businessUnitId
	 */
	public void sendEHSNotificationEmail(String[] emailIds, String subject, String mailBody, String businessUnitId) {
		Map<CommunicationChannel, String> senderMap = new HashMap<>();
		senderMap.put(CommunicationChannel.EMAIL, WebConstants.EHS_SENDER_EMAIL);
		M_Sender sender = new M_Sender(senderMap);

		List<M_Recipient> recipientList = getEmailRecipentList(emailIds);
		M_MessagePayload payload = M_MessagePayload.builder().addEmailPayload(subject, mailBody, true).build();
		M_Multicast multicastModel = M_Multicast
				.builder(businessUnitId, recipientList, payload, WebConstants.COMMUNICIATION_EVENT_NAME)
				.addSenderId(sender).build();

		try {
			multicastClient.send(multicastModel);
		} catch (JsonSyntaxException | IOException e) {
			logger.error(e.toString());
		}
	}

	/**
	 * 
	 * @param emailIds      : transport and security team email id
	 * @param vendorEmailId : vendor email id
	 * @return : list of all email IDs in the format
	 */
	public List<M_Recipient> getEmailRecipentList(String[] emailIds) {
		List<M_Recipient> recipientList = new ArrayList<M_Recipient>();
		for (String emailId : emailIds) {
			M_Recipient recipient = M_Recipient.builder(UUID.randomUUID().toString())
					.addEmailId(new M_EmailId(M_EmailId.EmailRecipientType.TO, emailId)).build();
			recipientList.add(recipient);
		}
		return recipientList;
	}

	/**
	 * 
	 * @param vendorEmailId         : vendor email ID
	 * @param emailIds              : transport and security team email ID
	 * @param mailSubject           : subject line of e-mail
	 * @param mailBody              : data need to be sent in bdy of e-mail
	 * @param communicationClientId : need to be populated in email structure
	 */
	public void sendEHSNotificationEmailToVendors(String vendorEmailId, String[] emailIds, String mailSubject,
			String mailBody, String communicationClientId) {
		Map<CommunicationChannel, String> senderMap = new HashMap<>();
		senderMap.put(CommunicationChannel.EMAIL, WebConstants.EHS_SENDER_EMAIL);
		M_Sender sender = new M_Sender(senderMap);

		List<M_Recipient> recipientList = getEmailRecipentList(emailIds, vendorEmailId);
		M_MessagePayload payload = M_MessagePayload.builder().addEmailPayload(mailSubject, mailBody, true).build();
		M_Multicast multicastModel = M_Multicast
				.builder(communicationClientId, recipientList, payload, WebConstants.COMMUNICIATION_EVENT_NAME)
				.addSenderId(sender).build();

		try {
			multicastClient.send(multicastModel);
		} catch (JsonSyntaxException | IOException e) {
			logger.error(e.toString());
		}

	}

	/**
	 * 
	 * @param emailIds      : transport and security team email id
	 * @param vendorEmailId : vendor email id
	 * @return : list of all email IDs in the format
	 */
	private List<M_Recipient> getEmailRecipentList(String[] emailIds, String vendorEmailId) {
		List<M_Recipient> recipientList = new ArrayList<M_Recipient>();
		for (String emailId : emailIds) {
			M_Recipient recipientCC = M_Recipient.builder(UUID.randomUUID().toString())
					.addEmailId(new M_EmailId(M_EmailId.EmailRecipientType.CC, emailId)).build();
			recipientList.add(recipientCC);
		}
		if(StringUtils.isNotBlank(vendorEmailId)) {
	        M_Recipient recipientTo = M_Recipient.builder(UUID.randomUUID().toString())
                .addEmailId(new M_EmailId(M_EmailId.EmailRecipientType.TO, vendorEmailId)).build();
	        recipientList.add(recipientTo);
		}
		return recipientList;
	}

}
