package com.moveinsync.compliance.ets.services.email;

import com.google.common.collect.Lists;
import com.moveinsync.compliance.dto.EHSAuditReportDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.ets.model.Vehicle;
import com.moveinsync.compliance.ets.services.service.VmsService;
import com.moveinsync.compliance.ets.web.util.EmailTemplateConstants;
import com.moveinsync.compliance.persistence.entities.EHSemailAlerts;
import com.moveinsync.compliance.persistence.repository.EHSAlertNotificationRepository;
import com.moveinsync.compliance.services.EHSReportingService;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class EHSComplianceEntryEmailNotificationService {

  @Autowired
  EHSReportingService eHSReportingService;

  @Autowired
  EHSAlertNotificationRepository eHSAlertNotificationRepository;

  @Autowired
  EHSCommunicationService eHSCommunicationService;

  @Autowired
  VmsService vmsService;

  static final Logger logger = LoggerFactory.getLogger(EHSComplianceEntryEmailNotificationService.class);

  public void sendAllowEntryEmailAlertNotification(VehicleEHSChecklistVO ehsStatusResponseVO,
      EHSAuditReportDTO ehsReportAudit) {
    List<String> emailIdList = Lists.newArrayList();
    List<Vehicle> vehicles = vmsService.getActiveVehicles(ehsStatusResponseVO.getBuid(),
        ehsStatusResponseVO.getRegistrationNumber());
    EHSemailAlerts eHSemailAlerts = eHSAlertNotificationRepository.findByBusinessUnitId(ehsStatusResponseVO.getBuid());
    if (!vehicles.isEmpty()) {
      for (Vehicle vehicle : vehicles) {
        if (ArrayUtils.isNotEmpty(vehicle.getVendorMailds())) {
          emailIdList.addAll(Arrays.asList(vehicle.getVendorMailds()));
        }
      }
    }
    if (eHSemailAlerts != null && StringUtils.isNotEmpty(eHSemailAlerts.getEmailIDsSequrityTeam())) {
      emailIdList.addAll(Arrays.asList(eHSemailAlerts.getEmailIDsSequrityTeam().split(",")));
    }
    if (eHSemailAlerts != null && StringUtils.isNotEmpty(eHSemailAlerts.getEmailIDsTransportTeam())) {
      emailIdList.addAll(Arrays.asList(eHSemailAlerts.getEmailIDsTransportTeam().split(",")));
    }
    String emails = StringUtils.join(emailIdList.toArray(), ',');
    if (StringUtils.isNotBlank(emails)) {
      String[] validMailId = BounceManager.INSTANCE.getValidEmails(emails.split(","));
      StringBuilder sb = prepareMailDataContent(ehsReportAudit);
      EmailTemplate template = new EmailTemplate(new Object[] { sb },
          EmailTemplateConstants.EHS_ALLOW_ENTRY_EMAIL_NOTIFICATION);
      if(ArrayUtils.isNotEmpty(validMailId)) {
        try {
          eHSCommunicationService.sendEHSNotificationEmail(validMailId,
              getMailSubject(ehsStatusResponseVO, ehsReportAudit), template.getMailBody(), ehsStatusResponseVO.getBuid());
        } catch (Exception ex) {
          logger.error(
              "Exception occured while sending instant block email alert failed for buID : {} vehicle Registration number : {} of emailIds : {}",
              ehsStatusResponseVO.getBuid(), ehsStatusResponseVO.getRegistrationNumber(), StringUtils.join(validMailId, ","));
        }
      } else {
        logger.info("Sending EHS instant block email alert failed due to emails bounced {}", emails);
      }
    }
  }

  private StringBuilder prepareMailDataContent(EHSAuditReportDTO ehsReportAudit) {
    String emailTableRows = "";
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<table border='1'>");
    stringBuilder.append(getTableHeadings());
    stringBuilder.append("<tbody>");
    emailTableRows = mailBodyData(ehsReportAudit);
    stringBuilder.append(emailTableRows);
    stringBuilder.append("</tbody></table>");
    return stringBuilder;
  }

  private String getTableHeadings() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<thead>");
    stringBuilder.append("<tr>");
    stringBuilder.append("<th>Change Time</th>");
    stringBuilder.append("<th>Vendor</th>");
    stringBuilder.append("<th>Registration</th>");
    stringBuilder.append("<th>Audited By</th>");
    stringBuilder.append("<th>Status</th>");
    stringBuilder.append("<th>Fields Rejected</th>");
    stringBuilder.append("<th>Comments</th>");
    stringBuilder.append("</thead>");
    return stringBuilder.toString();
  }

  private String mailBodyData(EHSAuditReportDTO ehsReportAudit) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<tr><td>");
    stringBuilder.append(ehsReportAudit.getChangeTime());
    stringBuilder.append("</td><td>");
    stringBuilder.append(ehsReportAudit.getVendorName());
    stringBuilder.append("</td><td>");
    stringBuilder.append(ehsReportAudit.getVehicleRegistration());
    stringBuilder.append("</td><td>");
    stringBuilder.append(ehsReportAudit.getAuditBy());
    stringBuilder.append("</td><td>");
    stringBuilder.append(ehsReportAudit.getOverallStatus());
    stringBuilder.append("</td><td>");
    List<String> fielsdsRejected = ehsReportAudit.getFieldsRejected();
    String fieldrejected = null;
    for (String subString : fielsdsRejected) {
      fieldrejected = fieldrejected + subString;
    }
    stringBuilder.append(fieldrejected);
    stringBuilder.append("</td><td>");
    stringBuilder.append(ehsReportAudit.getComment());
    return stringBuilder.toString();

  }

  private String getMailSubject(VehicleEHSChecklistVO ehsStatusResponseVO, EHSAuditReportDTO ehsReportAudit) {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("Vehicle Allow-Entry Notification : " + ehsStatusResponseVO.getRegistrationNumber() + " : "
        + ehsReportAudit.getVendorName()));
    return sb.toString();
  }
}
