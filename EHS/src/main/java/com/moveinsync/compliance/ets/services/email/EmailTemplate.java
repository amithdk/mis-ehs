package com.moveinsync.compliance.ets.services.email;

import com.mis.serverdata.utils.FileUtils;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;

public class EmailTemplate {

  private String subject;
  private String[] to;
  private Object[] args;
  private String template;
  private List<String> replyTo;

  public EmailTemplate() {
  }

  public EmailTemplate(String subject, String[] to, Object[] args, List<String> replyTo, String template) {
    this.subject = subject;
    this.to = to;
    this.args = args;
    this.template = template;
    this.replyTo = replyTo;
  }

  public EmailTemplate(String subject, String[] to, Object[] args, String template) {
    this.subject = subject;
    this.to = to;
    this.args = args;
    this.template = template;
  }
  
  public EmailTemplate(Object[] args, String template) {
    this.args = args;
    this.template = template;
  }
  
  private String getTemplate() {
    File file = null;
    ClassLoader classLoader = getClass().getClassLoader();
    try {
      file = new File(classLoader.getResource("/" + template).getFile());
      String path = file.getAbsolutePath();

      return FileUtils.readFile(path);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
  
  public String getMailBody() {
    String mailBody = MessageFormat.format(getTemplate(), args);
    return mailBody;
  }
}