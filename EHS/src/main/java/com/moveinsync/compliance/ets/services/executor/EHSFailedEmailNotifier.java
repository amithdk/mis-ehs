package com.moveinsync.compliance.ets.services.executor;

import com.moveinsync.compliance.dto.EHSAuditReportDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.services.EHSReportingService;

public class EHSFailedEmailNotifier implements Runnable {

  private EHSReportingService ehsReportingService;
  private VehicleEHSChecklistVO ehsStatusResponseVO;
  private String businessUnit;
  private String auditorEmail;
  private String auditorComment;
  private String[] mailRecipient;
  private String[] replyTo;
  private EHSAuditReportDTO ehsAuditReportDTO;

  public EHSFailedEmailNotifier(EHSReportingService ehsReportingService, VehicleEHSChecklistVO ehsStatusResponseVO,
      String businessUnit, String email, String comment, String[] to, String[] replyTo,
      EHSAuditReportDTO ehsAuditReportDTO) {
    this.ehsReportingService = ehsReportingService;
    this.ehsStatusResponseVO = ehsStatusResponseVO;
    this.businessUnit = businessUnit;
    this.auditorEmail = email;
    this.auditorComment = comment;
    this.mailRecipient = to;
    this.replyTo = replyTo;
    this.ehsAuditReportDTO = ehsAuditReportDTO;
  }

  @Override
  public void run() {
    execute();
  }

  public void execute() {
    ehsReportingService.sendFailedEHSNotification(ehsStatusResponseVO, ehsAuditReportDTO, businessUnit, mailRecipient, replyTo);
  }

}
