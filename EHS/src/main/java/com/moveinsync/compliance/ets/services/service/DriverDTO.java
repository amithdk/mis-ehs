package com.moveinsync.compliance.ets.services.service;

import com.moveinsync.compliance.models.BackgroundCheckStatus;

import java.util.Date;

public class DriverDTO {

  private String Name;

  private String vendorName;

  private String city;
  
  private String identifier;
  
  private Date inductionDate;

  private String gender;

  private String driverLicenseNumber;

  private Date licenseExpiryDate;

  private String badgeNumber;

  private Date badgeExpiryDate;

  private int driverAge;

  private BackgroundCheckStatus backgroundStatusCheck;

  private Boolean status;

  private String comments;

  private Date backgroundStatusUpdateTime;

  private Date bgvExpiryDate;

  private BackgroundCheckStatus policeVerificationStatus;

  private Date policeVerificationExpiryDate;

  private Date policeVerificationStatusUpdateTime;

  private boolean licenceValueNotAvailable;

  private boolean badgeValueNotAvailable;

  private boolean bgStatusValueNotAvailable;

  private boolean policeVerificationValueNotAvailable;

  private boolean ageValueNotAvailable;

  private String goveIdentificationType;

  private String govtIdentificationNumber;

  private String id;

  private String buid;

  private String phoneNumbers;

  private String address;
  
  private String currentAddress;

  private String profileRelativeUrl;
  private String licenceRelativeUrl;
  private String bgvRelativeUrl;
  private String policeVerficiationRelativeUrl;
  private String govtIdVerificationUrl;
  private boolean blacklisted;
  
  private boolean medicalValueNotAvailable;
  
  private BackgroundCheckStatus medicalVerificationStatus;
  
  private Date medicalVerificationExpiryDate;
  
  private String medicalRelativeUrl;
  
  private Date medicalVerificationStatusUpdateTime;
  
  private boolean trainingCertValueNotAvailable;
  
  private BackgroundCheckStatus trainingCertVerificationStatus;
  
  private Date trainingCertVerificationStatusUpdatedTime;
  
  private Date trainingCertVerificationExpiryDate;
  
  private String trainingCertRelativeUrl;
  
  private String louCertRelativeUrl;
  
  private Date createdTime;
  
  private Date updatedTime;

  private Date statusUpdatedTime;

  private Long dateOfBirth;

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public String getVendorName() {
    return vendorName;
  }

  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public Date getInductionDate() {
    return inductionDate;
  }

  public void setInductionDate(Date inductionDate) {
    this.inductionDate = inductionDate;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getDriverLicenseNumber() {
    return driverLicenseNumber;
  }

  public void setDriverLicenseNumber(String driverLicenseNumber) {
    this.driverLicenseNumber = driverLicenseNumber;
  }

  public Date getLicenseExpiryDate() {
    return licenseExpiryDate;
  }

  public void setLicenseExpiryDate(Date licenseExpiryDate) {
    this.licenseExpiryDate = licenseExpiryDate;
  }

  public String getBadgeNumber() {
    return badgeNumber;
  }

  public void setBadgeNumber(String badgeNumber) {
    this.badgeNumber = badgeNumber;
  }

  public Date getBadgeExpiryDate() {
    return badgeExpiryDate;
  }

  public void setBadgeExpiryDate(Date badgeExpiryDate) {
    this.badgeExpiryDate = badgeExpiryDate;
  }

  public int getDriverAge() {
    return driverAge;
  }

  public void setDriverAge(int driverAge) {
    this.driverAge = driverAge;
  }

  public BackgroundCheckStatus getBackgroundStatusCheck() {
    return backgroundStatusCheck;
  }

  public void setBackgroundStatusCheck(BackgroundCheckStatus backgroundStatusCheck) {
    this.backgroundStatusCheck = backgroundStatusCheck;
  }

  public Boolean getStatus() {
    return status;
  }

  public void setStatus(Boolean status) {
    this.status = status;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public Date getBackgroundStatusUpdateTime() {
    return backgroundStatusUpdateTime;
  }

  public void setBackgroundStatusUpdateTime(Date backgroundStatusUpdateTime) {
    this.backgroundStatusUpdateTime = backgroundStatusUpdateTime;
  }

  public Date getBgvExpiryDate() {
    return bgvExpiryDate;
  }

  public void setBgvExpiryDate(Date bgvExpiryDate) {
    this.bgvExpiryDate = bgvExpiryDate;
  }

  public BackgroundCheckStatus getPoliceVerificationStatus() {
    return policeVerificationStatus;
  }

  public void setPoliceVerificationStatus(BackgroundCheckStatus policeVerificationStatus) {
    this.policeVerificationStatus = policeVerificationStatus;
  }

  public Date getPoliceVerificationExpiryDate() {
    return policeVerificationExpiryDate;
  }

  public void setPoliceVerificationExpiryDate(Date policeVerificationExpiryDate) {
    this.policeVerificationExpiryDate = policeVerificationExpiryDate;
  }

  public Date getPoliceVerificationStatusUpdateTime() {
    return policeVerificationStatusUpdateTime;
  }

  public void setPoliceVerificationStatusUpdateTime(Date policeVerificationStatusUpdateTime) {
    this.policeVerificationStatusUpdateTime = policeVerificationStatusUpdateTime;
  }

  public boolean isLicenceValueNotAvailable() {
    return licenceValueNotAvailable;
  }

  public void setLicenceValueNotAvailable(boolean licenceValueNotAvailable) {
    this.licenceValueNotAvailable = licenceValueNotAvailable;
  }

  public boolean isBadgeValueNotAvailable() {
    return badgeValueNotAvailable;
  }

  public void setBadgeValueNotAvailable(boolean badgeValueNotAvailable) {
    this.badgeValueNotAvailable = badgeValueNotAvailable;
  }

  public boolean isBgStatusValueNotAvailable() {
    return bgStatusValueNotAvailable;
  }

  public void setBgStatusValueNotAvailable(boolean bgStatusValueNotAvailable) {
    this.bgStatusValueNotAvailable = bgStatusValueNotAvailable;
  }

  public boolean isPoliceVerificationValueNotAvailable() {
    return policeVerificationValueNotAvailable;
  }

  public void setPoliceVerificationValueNotAvailable(boolean policeVerificationValueNotAvailable) {
    this.policeVerificationValueNotAvailable = policeVerificationValueNotAvailable;
  }

  public boolean isAgeValueNotAvailable() {
    return ageValueNotAvailable;
  }

  public void setAgeValueNotAvailable(boolean ageValueNotAvailable) {
    this.ageValueNotAvailable = ageValueNotAvailable;
  }

  public String getGoveIdentificationType() {
    return goveIdentificationType;
  }

  public void setGoveIdentificationType(String goveIdentificationType) {
    this.goveIdentificationType = goveIdentificationType;
  }

  public String getGovtIdentificationNumber() {
    return govtIdentificationNumber;
  }

  public void setGovtIdentificationNumber(String govtIdentificationNumber) {
    this.govtIdentificationNumber = govtIdentificationNumber;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBuid() {
    return buid;
  }

  public void setBuid(String buid) {
    this.buid = buid;
  }

  public String getPhoneNumbers() {
    return phoneNumbers;
  }

  public void setPhoneNumbers(String phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCurrentAddress() {
    return currentAddress;
  }

  public void setCurrentAddress(String currentAddress) {
    this.currentAddress = currentAddress;
  }

  public String getProfileRelativeUrl() {
    return profileRelativeUrl;
  }

  public void setProfileRelativeUrl(String profileRelativeUrl) {
    this.profileRelativeUrl = profileRelativeUrl;
  }

  public String getLicenceRelativeUrl() {
    return licenceRelativeUrl;
  }

  public void setLicenceRelativeUrl(String licenceRelativeUrl) {
    this.licenceRelativeUrl = licenceRelativeUrl;
  }

  public String getBgvRelativeUrl() {
    return bgvRelativeUrl;
  }

  public void setBgvRelativeUrl(String bgvRelativeUrl) {
    this.bgvRelativeUrl = bgvRelativeUrl;
  }

  public String getPoliceVerficiationRelativeUrl() {
    return policeVerficiationRelativeUrl;
  }

  public void setPoliceVerficiationRelativeUrl(String policeVerficiationRelativeUrl) {
    this.policeVerficiationRelativeUrl = policeVerficiationRelativeUrl;
  }

  public String getGovtIdVerificationUrl() {
    return govtIdVerificationUrl;
  }

  public void setGovtIdVerificationUrl(String govtIdVerificationUrl) {
    this.govtIdVerificationUrl = govtIdVerificationUrl;
  }

  public boolean isBlacklisted() {
    return blacklisted;
  }

  public void setBlacklisted(boolean blacklisted) {
    this.blacklisted = blacklisted;
  }

  public boolean isMedicalValueNotAvailable() {
    return medicalValueNotAvailable;
  }

  public void setMedicalValueNotAvailable(boolean medicalValueNotAvailable) {
    this.medicalValueNotAvailable = medicalValueNotAvailable;
  }

  public BackgroundCheckStatus getMedicalVerificationStatus() {
    return medicalVerificationStatus;
  }

  public void setMedicalVerificationStatus(BackgroundCheckStatus medicalVerificationStatus) {
    this.medicalVerificationStatus = medicalVerificationStatus;
  }

  public Date getMedicalVerificationExpiryDate() {
    return medicalVerificationExpiryDate;
  }

  public void setMedicalVerificationExpiryDate(Date medicalVerificationExpiryDate) {
    this.medicalVerificationExpiryDate = medicalVerificationExpiryDate;
  }

  public String getMedicalRelativeUrl() {
    return medicalRelativeUrl;
  }

  public void setMedicalRelativeUrl(String medicalRelativeUrl) {
    this.medicalRelativeUrl = medicalRelativeUrl;
  }

  public Date getMedicalVerificationStatusUpdateTime() {
    return medicalVerificationStatusUpdateTime;
  }

  public void setMedicalVerificationStatusUpdateTime(Date medicalVerificationStatusUpdateTime) {
    this.medicalVerificationStatusUpdateTime = medicalVerificationStatusUpdateTime;
  }

  public boolean isTrainingCertValueNotAvailable() {
    return trainingCertValueNotAvailable;
  }

  public void setTrainingCertValueNotAvailable(boolean trainingCertValueNotAvailable) {
    this.trainingCertValueNotAvailable = trainingCertValueNotAvailable;
  }

  public BackgroundCheckStatus getTrainingCertVerificationStatus() {
    return trainingCertVerificationStatus;
  }

  public void setTrainingCertVerificationStatus(BackgroundCheckStatus trainingCertVerificationStatus) {
    this.trainingCertVerificationStatus = trainingCertVerificationStatus;
  }

  public Date getTrainingCertVerificationStatusUpdatedTime() {
    return trainingCertVerificationStatusUpdatedTime;
  }

  public void setTrainingCertVerificationStatusUpdatedTime(Date trainingCertVerificationStatusUpdatedTime) {
    this.trainingCertVerificationStatusUpdatedTime = trainingCertVerificationStatusUpdatedTime;
  }

  public Date getTrainingCertVerificationExpiryDate() {
    return trainingCertVerificationExpiryDate;
  }

  public void setTrainingCertVerificationExpiryDate(Date trainingCertVerificationExpiryDate) {
    this.trainingCertVerificationExpiryDate = trainingCertVerificationExpiryDate;
  }

  public String getTrainingCertRelativeUrl() {
    return trainingCertRelativeUrl;
  }

  public void setTrainingCertRelativeUrl(String trainingCertRelativeUrl) {
    this.trainingCertRelativeUrl = trainingCertRelativeUrl;
  }

  public String getLouCertRelativeUrl() {
    return louCertRelativeUrl;
  }

  public void setLouCertRelativeUrl(String louCertRelativeUrl) {
    this.louCertRelativeUrl = louCertRelativeUrl;
  }

  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }

  public Date getUpdatedTime() {
    return updatedTime;
  }

  public void setUpdatedTime(Date updatedTime) {
    this.updatedTime = updatedTime;
  }

  public Date getStatusUpdatedTime() {
    return statusUpdatedTime;
  }

  public void setStatusUpdatedTime(Date statusUpdatedTime) {
    this.statusUpdatedTime = statusUpdatedTime;
  }

  public Long getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Long dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }
}
