package com.moveinsync.compliance.ets.services.service;

import java.util.List;
import java.util.Map;

import com.moveinsync.compliance.ets.model.Vehicle;
import com.moveinsync.compliance.ets.model.VendorBuidDTO;
import com.moveinsync.vehiclemanagementservice.models.ETSCabTypeDTO;
import com.moveinsync.vehiclemanagementservice.models.VehicleDTO;
import com.moveinsync.vehiclemanagementservice.models.VendorDTO;

/**
 * @author Amith Soman
 * @Nov 13, 2014
 */
public interface VmsService {
	/**
	 * REST call to VehicleManagementSystem Fetch all vehicles for this Business
	 * Unit
	 *
	 */
	public Map<String, Vehicle> getVehiclesFromVMS(String businessUnitID);

	public Vehicle getVehicleFromVMS(String businessUnitID, String vehicleID, String etsVehicleID);

	public List<VehicleDTO> vmsCall(String businessUnitID);

	public Map<String, Map<String, String>> getVehicleIdRegMap(String buid);

	/**
	 * Method provide map of List of vehicles with vehicle registration number as
	 * key
	 *
	 * @param businessUnitId
	 * @return
	 *
	 */
	public Map<String, List<Vehicle>> getVehicles(String businessUnitId);

	public List<Vehicle> getVehicles(String businessUnit, String registrationNumber);

	public Map<String, ETSCabTypeDTO> getCabTypesFromVMS(String businessUnitID);

	public List<VendorDTO> getVendors(String businessUnitId);

	public Map<String, String> getVehicleIdRegistrationMap(String buid);

	public Map<String, Vehicle> getVehiclesMap(String buId);

	public VendorDTO getVendorById(String vendorGuId);

	public Vehicle getActiveVehicle(String businessUnitId, String registrationNumber);

	public List<Vehicle> getActiveVehicles(String businessUnitId, String registrationNumber);

	public Map<String, VendorDTO> getVendorMapByBuId(String businessUnitId);

	public Map<String, List<Vehicle>> getVehiclesByVendor(VendorBuidDTO vendorBuidDTO);

	public Map<String, Vehicle> getVehiclesFromVMSByVendor(VendorBuidDTO vendorBuidDTO);

	public Integer getCount(VendorBuidDTO vendorBuidDTO);

	public List<VehicleDTO> getVehicles(VendorBuidDTO vendorBuidDTO);

	public Map<String, List<Vehicle>> getVehiclesByVendorRN(VendorBuidDTO vendorBuidDTO);

	public Map<String, Vehicle> getVehiclesFromVMSByVendorRN(VendorBuidDTO vendorBuidDTO);

	public List<VehicleDTO> getVehiclesRN(VendorBuidDTO vendorBuidDTO);
	
	public Map<String, List<Vehicle>> getActiveVehicles(String businessUnitId);
	
	 public Map<String, Vehicle> getActiveVehiclesFromVMS(String businessUnitID);
	 
	 public List<VehicleDTO> vmsActiveVehiclesCall(String businessUnitID);
	 
	 public Map<String, List<Vehicle>> getInActiveVehicles(String businessUnitId);
	 
	 public Map<String, Vehicle> getInActiveVehiclesFromVMS(String businessUnitID);
	 
	 public List<VehicleDTO> vmsInActiveVehiclesCall(String businessUnitID);

}
