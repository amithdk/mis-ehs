package com.moveinsync.compliance.ets.services.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.moveinsync.compliance.ets.model.Vehicle;
import com.moveinsync.compliance.ets.model.VendorBuidDTO;
import com.moveinsync.compliance.ets.model.VmsDomainUrls;
import com.moveinsync.vehiclemanagementservice.models.ETSCabTypeDTO;
import com.moveinsync.vehiclemanagementservice.models.VehicleDTO;
import com.moveinsync.vehiclemanagementservice.models.VendorDTO;

/**
 * @author Amith Soman
 * @Nov 13, 2014
 */
@Service
public class VmsServiceImpl implements VmsService {
    

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    VmsDomainUrls vdu;
    static final Logger logger = LoggerFactory.getLogger(VmsServiceImpl.class);
    private Map<String, Map<String, ETSCabTypeDTO>> buWiseCabTypes = Maps.newHashMap();

    public enum ComplianceStatus {
      NEW, FINISHED
    }

    public enum Status {
      ACTIVE, INACTIVE, INVALID, VALID
    }
    
    public VmsServiceImpl() {
      RestTemplate restTemplate = new RestTemplate();
      List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
      messageConverters.add(new MappingJackson2HttpMessageConverter());
      restTemplate.setMessageConverters(messageConverters);
    }

    @Override
    public Vehicle getVehicleFromVMS(String businessUnitID, String vehicleID, String etsVehicleID) {
        Vehicle vehicle = null;
        // A simple GET request, the response will be mapped to VehicleDTO.class
        String restUrl = vdu.getDomain();
        restUrl = restUrl + vdu.getUrlVehicle();
        restUrl = restUrl.replace("{buid}", businessUnitID);
        restUrl = restUrl.replace("{vehicle_id}", vehicleID);

        try {
            VehicleDTO[] vehicleArray = restTemplate.getForObject(restUrl, VehicleDTO[].class);
            if (ArrayUtils.isEmpty(vehicleArray)) {
                return vehicle;
            }
            List<VehicleDTO> vdtoList = Arrays.asList(vehicleArray);
            if (!CollectionUtils.isEmpty(vdtoList)) {
                for (VehicleDTO vdto : vdtoList) {
                    if (etsVehicleID.equals(vdto.getVehicleDisplayId())) {
                        vehicle = populateVehicleDetails(vdto);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Failed to get the data from ems for id " + vehicleID + " bu " + businessUnitID, e);
        }
        return vehicle;
    }
    
    @Override
    public Map<String, List<Vehicle>> getActiveVehicles(String businessUnitId) {
      Map<String, Vehicle> vmsVehicles = getActiveVehiclesFromVMS(businessUnitId);
      Map<String, List<Vehicle>> vmsVehicleForCompliance = Maps.newHashMap();
      for (Vehicle vehicle : vmsVehicles.values()) {
        if (vmsVehicleForCompliance.containsKey(vehicle.getRegistartionNumber()) == false) {
          vmsVehicleForCompliance.put(vehicle.getRegistartionNumber(), new ArrayList<Vehicle>());
        }
        vmsVehicleForCompliance.get(vehicle.getRegistartionNumber()).add(vehicle);
        
      }
      return vmsVehicleForCompliance;
    }
    
    @Override
    public Map<String, List<Vehicle>> getInActiveVehicles(String businessUnitId) {
      Map<String, Vehicle> vmsVehicles = getInActiveVehiclesFromVMS(businessUnitId);
      Map<String, List<Vehicle>> vmsVehicleForCompliance = Maps.newHashMap();
      for (Vehicle vehicle : vmsVehicles.values()) {
        if (vmsVehicleForCompliance.containsKey(vehicle.getRegistartionNumber()) == false) {
          vmsVehicleForCompliance.put(vehicle.getRegistartionNumber(), new ArrayList<Vehicle>());
        }
        vmsVehicleForCompliance.get(vehicle.getRegistartionNumber()).add(vehicle);
        
      }
      return vmsVehicleForCompliance;
    }
    
    @Override
    public Map<String, Vehicle> getActiveVehiclesFromVMS(String businessUnitID) {
      Map<String, Vehicle> vehicleMap = new HashMap<>();
     
      List<VehicleDTO> vdtoList = vmsActiveVehiclesCall(businessUnitID);
      
      if (CollectionUtils.isEmpty(vdtoList)) {
        logger.error("vms did not return any data for bu " + businessUnitID);
        return vehicleMap;
      }

      for (VehicleDTO vdto : vdtoList) {
        if (StringUtils.isEmpty(vdto.getVehicleDisplayId())) {
          continue;
        }
        Vehicle vehicle = populateActiveVehicleDetails(vdto);
        if (vehicle.getEtsVehicleId() != null) {
          vehicleMap.put(vehicle.getEtsVehicleId(), vehicle);
        }
      }

      return vehicleMap;
    }
    
    @Override
    public Map<String, Vehicle> getInActiveVehiclesFromVMS(String businessUnitID) {
      Map<String, Vehicle> vehicleMap = new HashMap<>();
     
      List<VehicleDTO> vdtoList = vmsInActiveVehiclesCall(businessUnitID);
      
      if (CollectionUtils.isEmpty(vdtoList)) {
        logger.error("VMS did not return any data" + businessUnitID);
        return vehicleMap;
      }

      for (VehicleDTO vdto : vdtoList) {
        if (StringUtils.isEmpty(vdto.getVehicleDisplayId())) {
          continue;
        }
        Vehicle vehicle = populateInActiveVehicleDetails(vdto);
        if (vehicle.getEtsVehicleId() != null) {
          vehicleMap.put(vehicle.getEtsVehicleId(), vehicle);
        }
      }

      return vehicleMap;
    }
    
    private Vehicle populateActiveVehicleDetails(VehicleDTO vdto) {
	    Vehicle av = new Vehicle();
	    av.setCompliance(ComplianceStatus.NEW.toString());
	    av.setBusinessUnitID(vdto.getVendor().getBusinessUnitId());
	    av.setRegistartionNumber(vdto.getRegistration());
	    Boolean remoteActiveVal = Boolean.valueOf(vdto.isActive());
	    Date activeDate = vdto.getActivationDate();
	    String isActiveStatus = Status.ACTIVE.toString();
		/*
		 * if (activeDate == null || activeDate.after(Calendar.getInstance().getTime())
		 * == false) { isActiveStatus = remoteActiveVal.booleanValue() ?
		 * Status.ACTIVE.toString() : Status.INACTIVE.toString(); } else if
		 * (activeDate.after(Calendar.getInstance().getTime())) { isActiveStatus =
		 * remoteActiveVal.booleanValue() ? Status.INACTIVE.toString() :
		 * Status.ACTIVE.toString(); }
		 */
	    av.setStatus(isActiveStatus);
	    av.setActivationDate(vdto.getActivationDate());
	    av.setVehicleId(vdto.getKey());
	    av.setEtsVehicleId(vdto.getVehicleDisplayId());
	    VendorDTO vendor = vdto.getVendor();
	    if (vendor != null) {
	      String name = "Not Available";
	      if (vendor.getVendorName() != null) {

	        if (vdto.getVehicleDisplayId() == null || vdto.getVehicleDisplayId().contains(name)
	            || vdto.getVehicleDisplayId().contains(vendor.getVendorName())) {
	          name = vendor.getVendorName();
	        } else {
	          name = vdto.getVehicleDisplayId().substring(0, vdto.getVehicleDisplayId().indexOf("-"));
	        }
	      }
	      if (vendor.getEmailId() != null) {
	        av.setVendorMailds(vendor.getEmailId().split(","));
	      }
	      if (vendor.getVendorId() != null) {
	        av.setVendorId(vendor.getVendorId());
	      }
	      av.setVendor(name);
	    }
	    av.setVehicleType(vdto.getEtsCabType().getCabType());
	    av.setFuelType(vdto.getEtsCabType().getFuelType());
	    av.setVehicleTypeId(vdto.getEtsCabType().getEtsCabtTypeKey());

	    if (vdto.getCreatedTime() != 0) {
	      av.setCreatedTime(new Date(vdto.getCreatedTime()));
	    }
	    if (vdto.getUpdatedTime() != 0) {
	      av.setUpdatedTime(new Date(vdto.getUpdatedTime()));
	    }
	    av.setStatusCreatedTime(vdto.getStatusCreatedTime());
	    av.setStatusUpdatedTime(vdto.getStatusUpdatedTime());
	    av.setNativeVehicleId(vdto.getNativeVehicleId());
	    return av;
	  }
    
    private Vehicle populateInActiveVehicleDetails(VehicleDTO vdto) {
	    Vehicle av = new Vehicle();
	    av.setCompliance(ComplianceStatus.NEW.toString());
	    av.setBusinessUnitID(vdto.getVendor().getBusinessUnitId());
	    av.setRegistartionNumber(vdto.getRegistration());
	    Boolean remoteActiveVal = Boolean.valueOf(vdto.isActive());
	    Date activeDate = vdto.getActivationDate();
	    String isActiveStatus = Status.INACTIVE.toString();
		
	    av.setStatus(isActiveStatus);
	    av.setActivationDate(vdto.getActivationDate());
	    av.setVehicleId(vdto.getKey());
	    av.setEtsVehicleId(vdto.getVehicleDisplayId());
	    VendorDTO vendor = vdto.getVendor();
	    if (vendor != null) {
	      String name = "Not Available";
	      if (vendor.getVendorName() != null) {

	        if (vdto.getVehicleDisplayId() == null || vdto.getVehicleDisplayId().contains(name)
	            || vdto.getVehicleDisplayId().contains(vendor.getVendorName())) {
	          name = vendor.getVendorName();
	        } else {
	          name = vdto.getVehicleDisplayId().substring(0, vdto.getVehicleDisplayId().indexOf("-"));
	        }
	      }
	      if (vendor.getEmailId() != null) {
	        av.setVendorMailds(vendor.getEmailId().split(","));
	      }
	      if (vendor.getVendorId() != null) {
	        av.setVendorId(vendor.getVendorId());
	      }
	      av.setVendor(name);
	    }
	    av.setVehicleType(vdto.getEtsCabType().getCabType());
	    av.setFuelType(vdto.getEtsCabType().getFuelType());
	    av.setVehicleTypeId(vdto.getEtsCabType().getEtsCabtTypeKey());

	    if (vdto.getCreatedTime() != 0) {
	      av.setCreatedTime(new Date(vdto.getCreatedTime()));
	    }
	    if (vdto.getUpdatedTime() != 0) {
	      av.setUpdatedTime(new Date(vdto.getUpdatedTime()));
	    }
	    av.setStatusCreatedTime(vdto.getStatusCreatedTime());
	    av.setStatusUpdatedTime(vdto.getStatusUpdatedTime());
	    av.setNativeVehicleId(vdto.getNativeVehicleId());
	    return av;
	  }
    
    @Override
    public List<VehicleDTO> vmsActiveVehiclesCall(String businessUnitID) {
      String restUrl = vdu.getDomain();
      restUrl = restUrl + vdu.getUrlBuVehicles()+"/active";
      restUrl = restUrl.replace("{buid}", businessUnitID);
      List<VehicleDTO> vdtoList = null;
      try {
        VehicleDTO[] vehiclesArray = restTemplate.getForObject(restUrl, VehicleDTO[].class);
        if (ArrayUtils.isEmpty(vehiclesArray)) {
          return Lists.newArrayList();
        }
        vdtoList = Arrays.asList(vehiclesArray);
      } catch (Exception e) {
        logger.error("VMS call failed to get Vehicle"+ businessUnitID, e);
      }
      return vdtoList;
    }
    
    @Override
    public List<VehicleDTO> vmsInActiveVehiclesCall(String businessUnitID) {
      String restUrl = vdu.getDomain();
      restUrl = restUrl + vdu.getUrlBuVehicles()+"/inactive";
      restUrl = restUrl.replace("{buid}", businessUnitID);
      List<VehicleDTO> vdtoList = null;
      try {
        VehicleDTO[] vehiclesArray = restTemplate.getForObject(restUrl, VehicleDTO[].class);
        if (ArrayUtils.isEmpty(vehiclesArray)) {
          return Lists.newArrayList();
        }
        vdtoList = Arrays.asList(vehiclesArray);
      } catch (Exception e) {
        logger.error("VMS call failed to get Vehicle " + businessUnitID, e);
      }
      return vdtoList;
    }

    @Override
    public Map<String, Vehicle> getVehiclesFromVMS(String businessUnitID) {
        Map<String, Vehicle> avl = new HashMap<String, Vehicle>();

        List<VehicleDTO> vdtoList = vmsCall(businessUnitID);
        if (!CollectionUtils.isEmpty(vdtoList)) {
            for (VehicleDTO vdto : vdtoList) {
                if (StringUtils.isEmpty(vdto.getVehicleDisplayId())) {
                    continue;
                }
                Vehicle temp = populateVehicleDetails(vdto);
                if (temp.getEtsVehicleId() != null) {
                    avl.put(temp.getEtsVehicleId(), temp);
                }
            }
        } else {
            logger.error("VMS did not return any data " + businessUnitID);
        }

        return avl;
    }

    @Override
    public List<VehicleDTO> vmsCall(String businessUnitID) {
        String restUrl = vdu.getDomain();
        restUrl = restUrl + vdu.getUrlBuVehicles();
        restUrl = restUrl.replace("{buid}", businessUnitID);
        List<VehicleDTO> vdtoList = null;
        try {
            VehicleDTO[] vehiclesArray = restTemplate.getForObject(restUrl, VehicleDTO[].class);
            if (ArrayUtils.isEmpty(vehiclesArray)) {
                return Lists.newArrayList();
            }
            vdtoList = Arrays.asList(vehiclesArray);
        } catch (Exception e) {
            logger.error("VMS call failed to get Vehicle" + businessUnitID, e);
        }
        return vdtoList;
    }

    private Vehicle populateVehicleDetails(VehicleDTO vdto) {
        Vehicle av = new Vehicle();
        av.setCompliance(ComplianceStatus.NEW.toString());
        av.setBusinessUnitID(vdto.getVendor().getBusinessUnitId());
        av.setRegistartionNumber(vdto.getRegistration());
        Boolean remoteActiveVal = Boolean.valueOf(vdto.isActive());
        Date activeDate = vdto.getActivationDate();
        String isActiveStatus = Status.ACTIVE.toString();
        if (activeDate == null || activeDate.after(Calendar.getInstance().getTime()) == false) {
          isActiveStatus = remoteActiveVal.booleanValue() ? Status.ACTIVE.toString() : Status.INACTIVE.toString();
        } else if (activeDate.after(Calendar.getInstance().getTime())) {
          isActiveStatus = remoteActiveVal.booleanValue() ? Status.INACTIVE.toString() : Status.ACTIVE.toString();
        }
        av.setStatus(isActiveStatus);
        av.setActivationDate(vdto.getActivationDate());
        av.setVehicleId(vdto.getKey());
        av.setEtsVehicleId(vdto.getVehicleDisplayId());
        VendorDTO vendor = vdto.getVendor();
        if (vendor != null) {
          String name = "Not Available";
          if (vendor.getVendorName() != null) {

            if (vdto.getVehicleDisplayId() == null || vdto.getVehicleDisplayId().contains(name)
                || vdto.getVehicleDisplayId().contains(vendor.getVendorName())) {
              name = vendor.getVendorName();
            } else {
              name = vdto.getVehicleDisplayId().substring(0, vdto.getVehicleDisplayId().indexOf("-"));
            }
          }
          if (vendor.getEmailId() != null) {
            av.setVendorMailds(vendor.getEmailId().split(","));
          }
          if (vendor.getVendorId() != null) {
            av.setVendorId(vendor.getVendorId());
          }
          av.setVendor(name);
        }
        av.setVehicleType(vdto.getEtsCabType().getCabType());
        av.setFuelType(vdto.getEtsCabType().getFuelType());
        av.setVehicleTypeId(vdto.getEtsCabType().getEtsCabtTypeKey());

        if (vdto.getCreatedTime() != 0) {
          av.setCreatedTime(new Date(vdto.getCreatedTime()));
        }
        if (vdto.getUpdatedTime() != 0) {
          av.setUpdatedTime(new Date(vdto.getUpdatedTime()));
        }
        av.setStatusCreatedTime(vdto.getStatusCreatedTime());
        av.setStatusUpdatedTime(vdto.getStatusUpdatedTime());
        av.setNativeVehicleId(vdto.getNativeVehicleId());
        return av;
      }
  
  
  @Override
  public Map<String, Vehicle> getVehiclesFromVMSByVendor(VendorBuidDTO vendorBuidDTO) {
    Map<String, Vehicle> vehicleMap = new HashMap<String, Vehicle>();
   
    List<VehicleDTO> vdtoList = getVehiclesByBuidsAndVendor(vendorBuidDTO);
    
    if (CollectionUtils.isEmpty(vdtoList)) {
      logger.error("VMS did not return any data" + vendorBuidDTO.getBuidVendorMap());
      return vehicleMap;
    }

    for (VehicleDTO vdto : vdtoList) {
      if (StringUtils.isEmpty(vdto.getVehicleDisplayId())) {
        continue;
      }
      Vehicle vehicle = populateVehicleDetails(vdto);
      if (vehicle.getEtsVehicleId() != null) {
        vehicleMap.put(vehicle.getEtsVehicleId(), vehicle);
      }
    }

    return vehicleMap;
  }
  
  @Override
  public Map<String, Vehicle> getVehiclesFromVMSByVendorRN(VendorBuidDTO vendorBuidDTO) {
    Map<String, Vehicle> vehicleMap = new HashMap<String, Vehicle>();
   
    List<VehicleDTO> vdtoList = getVehiclesByBuidsAndVendorRN(vendorBuidDTO);
    
    if (CollectionUtils.isEmpty(vdtoList)) {
      logger.error("VMS did not return any data " + vendorBuidDTO.getBuidVendorMap());
      return vehicleMap;
    }

    for (VehicleDTO vdto : vdtoList) {
      if (StringUtils.isEmpty(vdto.getVehicleDisplayId())) {
        continue;
      }
      Vehicle vehicle = populateVehicleDetails(vdto);
      if (vehicle.getEtsVehicleId() != null) {
        vehicleMap.put(vehicle.getEtsVehicleId(), vehicle);
      }
    }

    return vehicleMap;
  }

  
  @Override
  public Map<String, List<Vehicle>> getVehiclesByVendor(VendorBuidDTO vendorBuidDTO) {
    Map<String, Vehicle> vmsVehicles = getVehiclesFromVMSByVendor(vendorBuidDTO);
    Map<String, List<Vehicle>> vmsVehicleForCompliance = Maps.newHashMap();
    for (Vehicle vehicle : vmsVehicles.values()) {
      if (vmsVehicleForCompliance.containsKey(vehicle.getRegistartionNumber()) == false) {
        vmsVehicleForCompliance.put(vehicle.getRegistartionNumber(), new ArrayList<Vehicle>());
      }
      vmsVehicleForCompliance.get(vehicle.getRegistartionNumber()).add(vehicle);
      
    }
    return vmsVehicleForCompliance;
  }
  
  @Override
  public Map<String, List<Vehicle>> getVehiclesByVendorRN(VendorBuidDTO vendorBuidDTO) {
    Map<String, Vehicle> vmsVehicles = getVehiclesFromVMSByVendorRN(vendorBuidDTO);
    Map<String, List<Vehicle>> vmsVehicleForCompliance = Maps.newHashMap();
    for (Vehicle vehicle : vmsVehicles.values()) {
      if (vmsVehicleForCompliance.containsKey(vehicle.getRegistartionNumber()) == false) {
        vmsVehicleForCompliance.put(vehicle.getRegistartionNumber(), new ArrayList<Vehicle>());
      }
      vmsVehicleForCompliance.get(vehicle.getRegistartionNumber()).add(vehicle);
      
    }
    return vmsVehicleForCompliance;
  }
  
  @Override
  public List<VehicleDTO> getVehicles(VendorBuidDTO vendorBuidDTO) {
   
	  List<VehicleDTO> vehicles = getVehiclesByBuidsAndVendor(vendorBuidDTO);
    return vehicles;
  }
  
  @Override
  public List<VehicleDTO> getVehiclesRN(VendorBuidDTO vendorBuidDTO) {
   
	  List<VehicleDTO> vehicles = getVehiclesByBuidsAndVendorRN(vendorBuidDTO);
    return vehicles;
  }
  
  @Override
  public Integer getCount(VendorBuidDTO vendorBuidDTO) {
    Integer count = getVehiclesCount(vendorBuidDTO);
    
    return count;
  }

    @Override
    public Map<String, List<Vehicle>> getVehicles(String businessUnitId) {
        Map<String, Vehicle> vmsVehicles = getVehiclesFromVMS(businessUnitId);
        Map<String, List<Vehicle>> vmsVehicleForCompliance = Maps.newHashMap();
        for (Vehicle vehicle : vmsVehicles.values()) {
            if (vmsVehicleForCompliance.containsKey(vehicle.getRegistartionNumber()) == false) {
                vmsVehicleForCompliance.put(vehicle.getRegistartionNumber(), new ArrayList<Vehicle>());
            }
            vmsVehicleForCompliance.get(vehicle.getRegistartionNumber()).add(vehicle);
        }
        return vmsVehicleForCompliance;
    }

    @Override
    public List<Vehicle> getVehicles(String businessUnit, String registrationNumber) {
        // A simple GET request, the response will be mapped to VehicleDTO.class
        URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain()).path("/").path(businessUnit)
                .path("/vehicles/registration/").path(registrationNumber).build().toUri();

        List<Vehicle> vehicles = Lists.newArrayList();
        try {
            VehicleDTO[] vehicleArray = restTemplate.getForObject(uri.toString(), VehicleDTO[].class);
            if (ArrayUtils.isEmpty(vehicleArray)) {
                return Lists.newArrayList();
            }
            List<VehicleDTO> vdtoList = Arrays.asList(vehicleArray);
            if (!CollectionUtils.isEmpty(vdtoList)) {
                for (VehicleDTO vdto : vdtoList) {
                    vehicles.add(populateVehicleDetails(vdto));
                }
            }
        } catch (Exception e) {
            logger.error("VMS call failed to get Vehicle with Registration " + registrationNumber, e);
        }
        return vehicles;
    }
  
  
  
  public List<VehicleDTO> getVehiclesByBuidsAndVendor(VendorBuidDTO vehicleDetailsDTO) {
    // A simple GET request, the response will be mapped to VehicleDTO.class
    URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain())
        .path("/allBuid/vehicles/vendors").build().toUri();

    List<VehicleDTO> vdtoList = Lists.newArrayList();
    try {
      VehicleDTO[] vehicleArray = restTemplate.postForObject(uri.toString(), vehicleDetailsDTO, VehicleDTO[].class);
      if (ArrayUtils.isEmpty(vehicleArray)) {
          logger.error("No vehicle data available", vehicleDetailsDTO.getBuidVendorMap());
          return Lists.newArrayList();
        }
        vdtoList = Arrays.asList(vehicleArray);

    } catch (Exception e) {
      logger.error("VMS call failed to get Vehicle with Registration" , e);
    }
    return vdtoList;
  }
  
  public List<VehicleDTO> getVehiclesByBuidsAndVendorRN(VendorBuidDTO vehicleDetailsDTO) {
	    URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain())
	        .path("/allBuid/vehicles/vendors/registration").build().toUri();

	    List<VehicleDTO> vdtoList = Lists.newArrayList();
	    try {
	      VehicleDTO[] vehicleArray = restTemplate.postForObject(uri.toString(), vehicleDetailsDTO, VehicleDTO[].class);
	      if (ArrayUtils.isEmpty(vehicleArray)) {
	          logger.error("No vehicle data available ", vehicleDetailsDTO.getBuidVendorMap());
	          return Lists.newArrayList();
	        }
	        vdtoList = Arrays.asList(vehicleArray);

	    } catch (Exception e) {
	      logger.error("VMS call failed to get Vehicle with Registration", e);
	    }
	    return vdtoList;
	  }
  
  
  
  public Integer getVehiclesCount(VendorBuidDTO vehicleDetailsDTO) {
	    URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain())
	        .path("/allBuid/vehicles/vendors/count").build().toUri();
	    Integer count=0;
	    try {
	      count = restTemplate.postForObject(uri.toString(), vehicleDetailsDTO, Integer.class);
	      

	    } catch (Exception e) {
	      logger.error("VMS call failed to get Vehicle with Registration", e);
	    }
	    return count;
	  }

    @Override
    public Map<String, ETSCabTypeDTO> getCabTypesFromVMS(String businessUnitID) {
        Map<String, ETSCabTypeDTO> cabTypeMap = new HashMap<String, ETSCabTypeDTO>();
        URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain()).path("/").path(businessUnitID).path("/vehicles/types")
                .build().toUri();
        try {
            ETSCabTypeDTO[] cabTypeArray = restTemplate.getForObject(uri.toString(), ETSCabTypeDTO[].class);
            for (ETSCabTypeDTO cabType : cabTypeArray) {
                cabTypeMap.put(cabType.getEtsCabtTypeKey(), cabType);
            }
        } catch (Exception e) {
            logger.error("VMS call failed to get ETS-CabTypes with businessUnitId " + businessUnitID, e);
        }
        return cabTypeMap;
    }

    @Override
    public List<VendorDTO> getVendors(String businessUnitId) {
        URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain()).path("/vendors/bu/").path(businessUnitId).build()
                .toUri();
        List<VendorDTO> vendorList = null;
        try {
            VendorDTO[] vendors = restTemplate.getForObject(uri.toString(), VendorDTO[].class);
            if (ArrayUtils.isEmpty(vendors)) {
                logger.info("No Vendor data available");
                return Lists.newArrayList();
            }
            vendorList = Arrays.asList(vendors);
        } catch (Exception e) {
            logger.error("VMS call failed to get Vendors", e);
        }
        return vendorList;
    }



    @Override
    public  Map<String,Map<String,String>> getVehicleIdRegMap(String buid) {
        Map<String, List<Vehicle>> vehicleDetailsMap = getVehicles(buid);
        Map<String,Map<String,String>> vehicleIdRegMap = Maps.newHashMap();
        for(Map.Entry<String,List<Vehicle>> entry : vehicleDetailsMap.entrySet()){
         if(!entry.getValue().get(0).getStatus().equals(Status.ACTIVE.name()))continue;
            Map<String,String> registrationVendorIdMap = Maps.newHashMap();
            registrationVendorIdMap.put(entry.getKey(), entry.getValue().get(0).getVendorId());
            vehicleIdRegMap.put(entry.getValue().get(0).getVehicleId(),registrationVendorIdMap);
        }
        return vehicleIdRegMap;
    }

    @Override
    public Map<String, String> getVehicleIdRegistrationMap(String buid) {
      Map<String, List<Vehicle>> vehicleDetailsMap = getVehicles(buid);
      Map<String,String> vehicleIdRegMap = Maps.newHashMap();
      for(Map.Entry<String,List<Vehicle>> entry : vehicleDetailsMap.entrySet()){
        vehicleIdRegMap.put(entry.getValue().get(0).getVehicleId(),entry.getKey());
    }
      return vehicleIdRegMap;
    }
    
  @Override
  public Map<String, Vehicle> getVehiclesMap(String buId) {
    Map<String, Vehicle> vmsVehicles = getVehiclesFromVMS(buId);
    Map<String, Vehicle> vmsVehicleForCompliance = Maps.newHashMap();
    for (Vehicle vehicle : vmsVehicles.values()) {
      if (vmsVehicleForCompliance.containsKey(vehicle.getVehicleId()) == true
          && vehicle.getStatus().equals(Status.INACTIVE.toString())) {
        continue;
      }
      vmsVehicleForCompliance.put(vehicle.getVehicleId(), vehicle);
    }
    return vmsVehicleForCompliance;
  }
  
  @Override
  public VendorDTO getVendorById(String vendorId) {
    URI uri = UriComponentsBuilder.fromUriString(vdu.getDomain()).path("/vendors/id/").path(vendorId).build().toUri();
    VendorDTO vendorDTO = null;
    try {
      vendorDTO = restTemplate.getForObject(uri.toString(), VendorDTO.class);
    } catch (Exception e) {
      logger.error("VMS call failed to get Vendors", e);
    }
    return vendorDTO;
  }
  
  @Override
  public Vehicle getActiveVehicle(String businessUnitId, String registrationNumber) {
    List<Vehicle> vehicles = getVehicles(businessUnitId, registrationNumber);
    for (Vehicle vehicle : vehicles) {
      if (StringUtils.equals(vehicle.getStatus(), Status.ACTIVE.name())) {
        return vehicle;
      }
    }
    return vehicles.get(0);
  }

  @Override
  public List<Vehicle> getActiveVehicles(String businessUnitId, String registrationNumber) {
    List<Vehicle> activeVehicles = Lists.newArrayList();
    List<Vehicle> vehicles = getVehicles(businessUnitId, registrationNumber);
    for (Vehicle vehicle : vehicles) {
      if (StringUtils.equals(vehicle.getStatus(), Status.ACTIVE.name())) {
        activeVehicles.add(vehicle);
      }
    }
    return activeVehicles;
  }

  @Override
  public Map<String, VendorDTO> getVendorMapByBuId(String businessUnitId) {
    List<VendorDTO> vendors = getVendors(businessUnitId);
    Map<String, VendorDTO> vendorMap = Maps.newHashMap();
    
    for(VendorDTO vendor : vendors) {
      vendorMap.put(vendor.getVendorName(), vendor);
    }
    return vendorMap;
  }
  
  

}
