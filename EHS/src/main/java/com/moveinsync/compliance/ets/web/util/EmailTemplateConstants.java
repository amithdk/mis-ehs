package com.moveinsync.compliance.ets.web.util;

public class EmailTemplateConstants {

  public static final String DAILY_VEHICLE_ALERT_NOTIFICATION_TEMPLATE = "schedule_change_notification.txt";
  public static final String DAILY_VEHICLE_SERVICE_ALERT_NOTIFICATION_TEMPLATE = "Vehicle_Service_Notification.txt";
  
  public static final String DAILY_DRIVER_ALERT_NOTIFICATION_TEMPLATE = "driver_compliance_notification.txt";

  public static final String COMPLIANCE_APPROVED_FIELDS_NOTIFICATION_TEMPLATE = "compliance_approved_fields_daily_notification.txt";
  public static final String COMPLIANCE_REJECTED_FIELDS_NOTIFICATION_TEMPLATE = "compliance_rejected_fields_daily_notification.txt";

  public static final String DRIVER_APPROVAL_FAILED_NOTIFICATION_TEMPLATE = "driver_approval_failed_notification.txt";
  
  public static final String VEHICLE_APPROVAL_FAILED_NOTIFICATION_TEMPLATE = "vehicle_approval_failed_notification.txt";
  
  public static final String BLACKLISTED_DRIVER_NOTIFCAITON_TEMPLATE = "blacklisted_driver_notification.txt";
  
  public static final String EHS_INSTANT_FAILED_EMAIL_NOTIFICATION_TEMPLATE = "ehs_failed_email_notification.txt";
  
  public static final String EHS_DIGEST_EMAIL_NOTIFICATION = "ehs_email_digest_notification.txt";
  
  public static final String EHS_ALLOW_ENTRY_EMAIL_NOTIFICATION = "ehs_email_allow_entry_notification.txt";
  
  public static final String COMPLIANCE_COLUMN_CONFIGURATION_NOTIFICATION = "compliance_column_configuration_notification.txt";
  public static final String VEHICLE_COMPLIANCE_CONFIGURATION_NOTIFICATION = "vehicle_compliance_configuration_notification.txt";
  public static final String DRIVER_COMPLIANCE_CRITERIA_CONFIGURATION_NOTIFICATION = "driver_compliance_criteria_configuration_notification.txt";
  public static final String DRIVER_COMPLIANCE_ALERT_CONFIGURATION_NOTIFICATION = "driver_compliance_alert_configuration_notification.txt";
}
