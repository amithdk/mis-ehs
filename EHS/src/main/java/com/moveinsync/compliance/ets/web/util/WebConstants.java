package com.moveinsync.compliance.ets.web.util;

import java.text.SimpleDateFormat;

/**
 * @author Amith Soman
 * @Oct 24, 2014
 */

public class WebConstants {
  /**
   * Views
   */

  // public static final String VIEW_VEHICLE_COMPLIANCE_DETAILS = "addvehiclecompliancedata";

  public static final String VIEW_VEHICLE_COMPLIANCE_ALERTS = "addvehiclealertsdata";
  
  public static final String PENALTY_CHECKLIST = "/penaltyCheckList";

  public static final String VIEW_VEHICLE_COMLIANCE_CRITERIA = "addvehiclecompliancecriteria";

  public static final String VIEW_VEHICLE_ALERTS_DETAILS = "vehiclealertsemailform";

  public static final String VIEW_RULES_MANAGEMENT = "rulesmanagement/rules-management";

  public static final String VIEW_RULES_EDIT_FORM = "rulesmanagement/rules-edit-form";

  public static final String REDIRECT_TO_HOMEPAGE_FROM_COMPLIANCE = "dashboard/dashboard";

  public static final String REDIRECT_TO_SUCCESS_PAGE_FROM_COMPLIANCE = "dashboard/dashboard";

  public static final String VIEW_VEHICLE_COMPLIANCE_HOME = "compliance/vehiclecompliance";

  public static final String VIEW_VEHICLE_ALERTS_HOME = "alerts/vehiclealerts";

  public static final String VIEW_COMPLIANCE_DOCUMENTS = "compliance/complianceview";

  public static final String VIEW_VEHICLE_ALERTS_DASHBOARD = "alerts/alertsdashboard";

  public static final String VIEW_VEHICLE_DASHBOARD_HOME = "dashboard/dashboard";

  public static final String REDIRECT_N0_BU_ID_REQUEST = "dashboard/unsuccessful";

  public static final String UNSUCCESSFUL_VIEW = "unsuccessful";

  public static final String VIEW_VEHICLE_AUDIT_HISTORY = "compliance/vehicleaudit";

  public static final String VIEW_COMPLIANCE_APPROVAL_DASHBOARD = "dashboard/approval";

  public static final String VIEW_VEHICLE_COMPLIANCE_APPROVAL_DASHBOARD = "dashboard/vehicle-approval-view";

  public static final String VIEW_DRIVER_COMPLIANCE_APPROVAL_DASHBOARD = "dashboard/driver-approval-view";

  public static final String VIEW_APPROVAL_AUDIT_HISTORY = "compliance/approvalaudit";

  public static final String VIEW_VEHICLE_APPROVAL_AUDIT_HISTORY = "compliance/vehicleapprovalaudit";

  public static final String VIEW_DRIVER_APPROVAL_AUDIT_HISTORY = "compliance/driverapprovalaudit";
  
  public static final String REDIRECT_ALERTS_DASHBOARD = "redirect:/alerts/dashboard";

  /**
   * Controllers
   */
  public static final String CONTROLLER_COMPLIANCE = "/compliance";

  public static final String CONTROLLER_RULES = "/rules";

  public static final String CONTROLLER_DASHBOARD = "/dashboard";

  public static final String CONTROLLER_ALERTS = "/alerts";

  public static final String CONTROLLER_DRIVER = "/rest/drivers";

  public static final String CONTROLLER_AUDIT = "/audit";

  public static final String CONTROLLER_CHECK_BOX = "/config";

  public static final String CONTROLLER_COMPLIANCE_APPROVAL_DASHBOARD = "/approval";

  public static final String APPROVAL_AUDIT = "/approvalaudit";

  /**
   * Session Objects
   */

  public static final String SESSION_ALERTS_VIEW_OBJECT = "alertsVO";

  public static final String SESSION_DASHBOARD_VIEW_OBJECT = "complianceVehicles";
  
  public static final String SESSION_DASHBOARD_INACTIVE_VIEW_OBJECT = "inactivevehicle";

  public static final String SESSION_COLUMN_CONFIG = "columnConfig";

  public static final int SESSION_TIME_OUT_PARAMETER = 2700;

  public static final String SESSION_REFERRER = "referer";

  public static final String SESSION_USER_NAME = "name";

  public static final String SESSION_USER_ROLE = "roleId";

  public static final String SESSION_VENDOR_ID = "vendorGuId";

  public static final String MUST_BE_COMING_FROM = "moveinsync.com";
  
  public static final String EHS_EMAIL_ALERT= "/ehsEmailAlert";

  /**
   * Request Objects
   */
  public static final String BUSINESS_UNIT_ID = "buId";

  public static final String ORGANIZATION_NAME = "org";

  public static final String RULE_TEMPALTES = "ruleTemplates";

  public static final String RULE_FOR_EDIT = "rule";

  public static final String RULE_VALUE_VALIDATION_DATA = "ruleValValData";

  public static final String VEHICLE_ID = "vehicleid";

  public static final String ETS_ID = "etsid";

  public static final String REGISTRATION_NUMBER = "regnum";

  public static final String REQUEST_SHOW_VIEW_VALUE = "displayincludedpage";

  public static final String REQUEST_ALERTS_VIEW_OBJECT = "alertsVoReq";

  public static final String REQUEST_COMPLIANCE_VIEW_OBJECT = "complianceVO";

  public static final String REQUEST_COMPLIANCE_AUDIT_SEARCH_VO = "vehicleAuditVO";

  public static final String REQUEST_COMPLIANCE_AUDIT_DATA = "vehicleAuditData";

  public static final String REQUEST_COLUMN_CONFIG_VO = "columnConfigVO";

  public static final String REQUEST_SELECTED_VEHICLE_LIST = "selectedVehicleList";

  public static final String REQUEST_APPROVAL_AUDIT_SEARCH_VO = "approvalAuditVO";

  public static final String REQUEST_VEHICLE_APPROVAL_AUDIT_SEARCH_VO = "vehicleApprovalAuditVO";

  public static final String REQUEST_DRIVER_APPROVAL_AUDIT_SEARCH_VO = "driverApprovalAuditVO";

  public static final String REQUEST_USER_NAME = "name";

  /**
   * EHS controller end-points
   */
  public static final String EHS_CONTROLLER = "/rest/ehs";

  public static final String SAVE_VEHICLE_EHS_DETAILS = "/vehicles/{vehicleId}";

  public static final String VEHICLES_FOR_BUSINESS_UNIT = "/vehicles";

  public static final String GENERATE_CHECKLIST = "/checklist";

  public static final String CHECKLIST_FOR_BUSINESS_UNIT = "/checklist/{buId}";

  public static final String VEHICLE_EHS_DETAILS = "/{buId}/vehicles/{vehicleId}";

  public static final String OVERALL_AUDIT_REPORT = "/audit/{buId}";

  public static final String VEHICLE_AUDIT_REPORT = "/audit/{buId}/{vehicleid}/{from}/{upto}";

  public static final String SAVE_PENALTY_CONFIGURATIONS = "/penalty/{buId}";

  public static final String ALL_VEHICLES = "ALL";

  /**
   * Other constants
   */
  public static final String ERRORS = "errors";

  public static final String OVERALL_STATUS_ID = "OVERALL";

  public static final String PREVIOUS_ACTION = "previousAction";

  public static final String NEXT_PAGE = "Next";

  public static final String PREVIOUS_PAGE = "Previous";

  public static final String FINISH_PAGE = "Submit";

  public static final String CANCEL_PAGE = "Cancel";

  public static final String EXIT = "exit";

  public static final String MODE = "mode";

  public static final String FINISH_PAGE_COMPLIANCE = "Finish";

  public static final String ACTIVE_PAGE = "active";

  public static final long MILLISECONDS_IN_A_WEEK = 604800000;

  public static final String EXCEL_SHEET_LABEL = "ComplianceSheet";

  public static final int EXCEL_CONSUMABLE_ARRAY_SIZE_ACTIVE = 23;

  public static final int EXCEL_CONSUMABLE_ARRAY_SIZE_PENDING = 4;

  public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

  public static final String DD_MM_YYYY = "dd/MM/yyyy";

  public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");

  public static final int BULK_UPLOAD_FILE_PROCESSOR_SIZE = 13;
  public static final int DOWNLOADED_UPLOAD_FILE_PROCESSOR_SIZE = 18;

  public static final String DOMAIN_NAME = "moveinsync.com";

  /** Approval Dashboard Constants */
  public static final String RoadTax_LifeTime = "Lifetime";
  public static final String APPROVAL_DASHBOARD_BUID_AVIALABLE = "approvalDashboardBuId";

  public static final String DOCUMENT_CHANGED = "Document Changed";
  public static final String DOCUMENT_NOT_CHANGED = "Document Not Changed";

  public static final String IDENTIFICATION_NUMBER_NOT_AVAILABLE_STRING = "NA";
  public static final String[] APPROVAL_SUMMARY_EMAIL_IDS = { "rajesh.mv@moveinsync.com",
      "ramesh.kumar@moveinsync.com" };
  public static final String DOCUMENT_NOT_AVAILABLE = "NOT AVAILABLE";

  /** Driver Constants */
  public static final String DRIVER_COMPLIANCE_CONFIGURATION_VIEW = "driverComplianceFormVO";
  public static final String REQUEST_DRIVER_ALERT_VIEW_OBJECT = "driverComplianceFormVO";
  public static final String REQUEST_SELECTED_DRIVER_LIST = "selectedDriverList";

  public static final String DRIVER_DATA = "driverCO";
  public static final String DRIVER_COMPLAINCE_DASHBOARD_DATA_ATTRIBUTE = "driverCompliance";
  
  public static final String DRIVER_COMPLAINCE_INACTIVE_DASHBOARD_DATA_ATTRIBUTE = "driversIncative";

  public static final String COMPLIANT = "Compliant";
  public static final String NON_COMPLIANT = "Non-Compliant";
  public static final String APPROACHING_NON_COMPLIANT = "Approaching-Non-Compliance";

  public static final int EXCEL_CONSUMABLE_DRIVER_ARRAY_SIZE_ACTIVE = 27;
  public static final String VEHICLE_COMPLIANCE_FILE_PATH = "/mnt/local/deploy/compliancetempfile";
  public static final String REQUESTOR_URL = "REQUESTOR-URL";
  public static final String EMAIL_BOUNCE_SERVER_URI = "http://bounces.moveinsync.com:10103/";

  /** AWS Credentials */
  public static final String ACCESS_KEY_ID = "AKIAJ6ZUWVGJXGLTLXFQ";
  public static final String SECRET_KEY = "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";

  /** Redis Cache Keys */
  public static final String DRIVER_OVERALL_COMPLIANCE_STATUS = "cs_driver_overall_compliance_status";
  public static final String DRIVER_COMPLIANCE_DASHBOARD_CONFIGURATION = "cs_driver_compliance_dashboard_config";
  public static final String DRIVER_CONFIGURATION = "cs_driver_configuration";
  public static final String DRIVER_COMPLIANCE_ALERT_CONFIGURATION = "cs_driver_compliance_alert_config";
  public static final String COLUMN_CONFIGURATION = "cs_column_config";
  public static final String ALERTS = "cs_alerts";
  public static final String VEHICLE_TYPE_ALERT = "cs_vehicle_type_alert";
  public static final String VEHICLE_COMPLIANCE = "cs_vehicle_compliance";
  public static final String VEHICLE_COLUMN_TYPE = "vehicle";
  public static final String DRIVER_COLUMN_TYPE = "driver";

  public static final String SSO_COOKIE_FOR_COMPLIANCE = "sso_compliance";
  public static final String BUSINESS_UNIT_ID_COOKIE = "businessUnitId";

  /** Communication Client Properties **/
  public static final String SENDER_EMAIL = "transport@moveinsync.com";
  public static final String COMMUNICIATION_EVENT_NAME = "EMAIL";
  public static final String COMMUNICATION_CLIENT_ID = "mis-compliance";
  public static final String EHS_SENDER_EMAIL = "central-compliance@moveinsync.com";

  /* EHS Audit Texts */
  public static final String COMMENT_EXPIRED = "EHS EXPIRED !";
  public static final String COMMENT_APPROACHING = "EHS APRROACHING EXPIRY !";

  /* S3 Bucket Names */
  public static final String DRIVER_DOCUMENT_BUCKET_NAME = "driver-document";
  public static final String VEHICLE_DOCUMENT_BUCKET_NAME = "vehicle-documents";

  /* Status Colors */
  public static final String APPROACHING_NON_COMPLIANT_STATUS_COLOR = "#f1c40f";
  public static final String NON_COMPLIANT_STATUS_COLOR = "IndianRed";
  public static final String COMPLIANT_STATUS_COLOR = "none";

  public static final String MIS_TOKEN_KEY = "x-mis-token";
  public static final String MIS_TOKEN_VALUE = "Rf2zAClKVcvlYGCz1S2h";
  
  public static final int COMPLIANCE_MANAGER_ROLE_ID = 25;
  public static final int COMPLIANCE_AUDITOR_ROLE_ID = 29;
  
  public static final String ENABLED = "Enabled";
  public static final String NOT_ENABLED = "Not Enabled";
}
