package com.moveinsync.compliance.ets.web.viewobject;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BUVehicleChecklistVO {

  private String checkListName;

  @JsonProperty
  private boolean isMandatory;

  private int sequence;
  
  private String adjustment;

  public String getCheckListName() {
    return checkListName;
  }

  public void setCheckListName(String checkListName) {
    this.checkListName = checkListName;
  }

  public boolean isMandatory() {
    return isMandatory;
  }

  public void setMandatory(boolean isMandatory) {
    this.isMandatory = isMandatory;
  }

  public int getSequence() {
    return sequence;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

  public String getAdjustment() {
    return adjustment;
  }

  public void setAdjustment(String adjustment) {
    this.adjustment = adjustment;
  }

}
