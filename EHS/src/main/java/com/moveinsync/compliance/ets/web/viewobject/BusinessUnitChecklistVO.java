package com.moveinsync.compliance.ets.web.viewobject;

public class BusinessUnitChecklistVO implements Comparable<BusinessUnitChecklistVO>{

  private String checklistId;

  private String name;

  private String iconPath;

  private String type;

  private String hintText;

  private boolean isMandatory;

  private int sequence;
  
  public BusinessUnitChecklistVO() { }

  @Override
  public int compareTo(BusinessUnitChecklistVO buVo) {
      return new Integer(this.sequence).compareTo(new Integer(buVo.sequence));
  }
  
  public String getChecklistId() {
    return checklistId;
  }

  public void setChecklistId(String checklistId) {
    this.checklistId = checklistId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIconPath() {
    return iconPath;
  }

  public void setIconPath(String iconPath) {
    this.iconPath = iconPath;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getHintText() {
    return hintText;
  }

  public void setHintText(String hintText) {
    this.hintText = hintText;
  }

  public boolean isMandatory() {
    return isMandatory;
  }

  public void setMandatory(boolean isMandatory) {
    this.isMandatory = isMandatory;
  }

  public int getSequence() {
    return sequence;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

}
