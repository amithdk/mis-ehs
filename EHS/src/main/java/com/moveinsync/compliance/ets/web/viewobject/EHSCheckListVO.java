package com.moveinsync.compliance.ets.web.viewobject;

public class EHSCheckListVO {

  private String checkListName;

  private String type;

  private String iconPath;

  private String hintText;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIconPath() {
    return iconPath;
  }

  public void setIconPath(String iconPath) {
    this.iconPath = iconPath;
  }

  public String getHintText() {
    return hintText;
  }

  public void setHintText(String hintText) {
    this.hintText = hintText;
  }

  public String getCheckListName() {
    return checkListName;
  }

  public void setCheckListName(String checkListName) {
    this.checkListName = checkListName;
  }

}
