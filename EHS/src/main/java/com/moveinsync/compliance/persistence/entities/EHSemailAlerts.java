package com.moveinsync.compliance.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ehs_email_alert_status")
public class EHSemailAlerts implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "business_unit_id")
	private String buId;
	@Column(name = "ehs_email_ids_transport_team")
	private String emailIDsTransportTeam;
	@Column(name = "ehs_email_ids_sequrity_team")
	private String emailIDsSequrityTeam;
	@Column(name = "ehs_daily_digest")
	private boolean dailyDigest;
	@Column(name = "ehs_approaching_expiry")
	private boolean approachingExpiry;
	@Column(name = "ehs_failed_notification")
	private boolean failedNotification;
	@Column(name = "ehs_send_to_vendors")
	private boolean sendToVendors;
	public String getBuId() {
		return buId;
	}
	public void setBuId(String buId) {
		this.buId = buId;
	}
	public String getEmailIDsTransportTeam() {
		return emailIDsTransportTeam;
	}
	public void setEmailIDsTransportTeam(String emailIDsTransportTeam) {
		this.emailIDsTransportTeam = emailIDsTransportTeam;
	}
	public String getEmailIDsSequrityTeam() {
		return emailIDsSequrityTeam;
	}
	public void setEmailIDsSequrityTeam(String emailIDsSequrityTeam) {
		this.emailIDsSequrityTeam = emailIDsSequrityTeam;
	}
	public boolean isDailyDigest() {
		return dailyDigest;
	}
	public void setDailyDigest(boolean dailyDigest) {
		this.dailyDigest = dailyDigest;
	}
	public boolean isApproachingExpiry() {
		return approachingExpiry;
	}
	public void setApproachingExpiry(boolean approachingExpiry) {
		this.approachingExpiry = approachingExpiry;
	}
	public boolean isFailedNotification() {
		return failedNotification;
	}
	public void setFailedNotification(boolean failedNotification) {
		this.failedNotification = failedNotification;
	}
	public boolean isSendToVendors() {
		return sendToVendors;
	}
	public void setSendToVendors(boolean sendToVendors) {
		this.sendToVendors = sendToVendors;
	}
	@Override
	public String toString() {
		return "EHSemailAlerts [buId=" + buId + ", emailIDsTransportTeam=" + emailIDsTransportTeam
				+ ", emailIDsSequrityTeam=" + emailIDsSequrityTeam + ", dailyDigest=" + dailyDigest
				+ ", approachingExpiry=" + approachingExpiry + ", failedNotification=" + failedNotification
				+ ", sendToVendors=" + sendToVendors + "]";
	}
}
