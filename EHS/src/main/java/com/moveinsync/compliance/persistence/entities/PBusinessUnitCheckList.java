package com.moveinsync.compliance.persistence.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * @author rajanish
 *
 */

@Entity
@Table(name = "business_unit_checklists")
public class PBusinessUnitCheckList {

  @EmbeddedId
  private PBusinessUnitCheckListId id;

  @Column(name = "is_mandatory")
  private boolean isMandatory;

  @Column(name = "sequence")
  private int sequence;

  @Column(name = "adjustment")
  private String adjustment;
  
  @Column(name = "ehs_audit_enabled")
  private boolean ehsAuditEnabled;

  public PBusinessUnitCheckList(String buid, String checklistId) {
    this.id = new PBusinessUnitCheckListId(buid, checklistId);
  }

  PBusinessUnitCheckList() {
  }

  public PBusinessUnitCheckListId getId() {
    return id;
  }

  public boolean isMandatory() {
    return isMandatory;
  }

  public int getSequence() {
    return sequence;
  }

  public String getAdjustment() {
    return adjustment;
  }

  public void setAdjustment(String adjustment) {
    this.adjustment = adjustment;
  }

  public void setMandatory(boolean isMandatory) {
    this.isMandatory = isMandatory;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

  public boolean isEhsAuditEnabled() {
    return ehsAuditEnabled;
  }

  public void setEhsAuditEnabled(boolean ehsAuditEnabled) {
    this.ehsAuditEnabled = ehsAuditEnabled;
  }

}