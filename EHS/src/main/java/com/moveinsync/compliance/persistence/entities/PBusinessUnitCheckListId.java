package com.moveinsync.compliance.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PBusinessUnitCheckListId implements Serializable {

  /**
   * @author rajanish
   * 
   */
  private static final long serialVersionUID = -2672215296971007266L;

  @Column(name = "business_unit_id")
  private String buid;

  @Column(name = "checklist_id")
  private String checklistId;

  public PBusinessUnitCheckListId(String buid, String checklistId) {
    this.buid = buid;
    this.checklistId = checklistId;
  }

  PBusinessUnitCheckListId() {
  }

  public String getBuid() {
    return buid;
  }

  public String getChecklistId() {
    return checklistId;
  }

}