package com.moveinsync.compliance.persistence.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ehs_additional_log")
public class PEHSAdditionalLog {

  @Column(name = "business_unit_id")
  private String businessUnit;

  @Column(name = "vehicle_id")
  private String vehicleId;

  @Id
  @Column(name = "update_time")
  private Date updateTime;

  @Column(name = "facility")
  private String facility;

  @Column(name = "driver_name")
  private String driverName;

  @Column(name = "kms_travelled")
  private String kmsTravelled;

  public PEHSAdditionalLog() {
  }

  public String getFacility() {
    return facility;
  }

  public void setFacility(String facility) {
    this.facility = facility;
  }

  public String getDriverName() {
    return driverName;
  }

  public void setDriverName(String driverName) {
    this.driverName = driverName;
  }

  public String getKmsTravelled() {
    return kmsTravelled;
  }

  public void setKmsTravelled(String kmsTravelled) {
    this.kmsTravelled = kmsTravelled;
  }

  public String getBusinessUnit() {
    return businessUnit;
  }

  public void setBusinessUnit(String businessUnit) {
    this.businessUnit = businessUnit;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
