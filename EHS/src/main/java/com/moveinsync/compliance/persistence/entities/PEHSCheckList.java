package com.moveinsync.compliance.persistence.entities;

import com.moveinsync.utils.EntityIdGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

/**
 * 
 * @author rajanish
 *
 */

@Entity
@Table(name = "ehs_checklists")
public class PEHSCheckList {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "name")
  private String name;

  @Column(name = "icon_path")
  private String iconPath;

  @Column(name = "type")
  private String type;

  @Column(name = "hint_text")
  private String hintText;

  PEHSCheckList() {
  }

  public void setId(String id) {
    this.id = id;
  }

  public PEHSCheckList(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public String getIconPath() {
    return iconPath;
  }

  public String getType() {
    return type;
  }

  public String getId() {
    return id;
  }

  public String getHintText() {
    return hintText;
  }

  public void setIconPath(String iconPath) {
    this.iconPath = iconPath;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setHintText(String hintText) {
    this.hintText = hintText;
  }

  @PrePersist
  void doBeforePersist() {
    if (id == null) {
      id = EntityIdGenerator.generateGUID(getEntityPrefix());
    }
  }

  private String getEntityPrefix() {
    return "CHK";
  }

}
