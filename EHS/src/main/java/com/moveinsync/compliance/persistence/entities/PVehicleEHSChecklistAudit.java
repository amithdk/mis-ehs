package com.moveinsync.compliance.persistence.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moveinsync.compliance.ets.persistence.entities.RevInfo;

@Entity
@Table(name = "vehicle_ehs_checklist_status_aud")
public class PVehicleEHSChecklistAudit {

  @EmbeddedId
  private PVehicleEHSChecklistAuditId vehicleEHSAuditId;

  @Column(name = "status")
  private String ehsStatus;

  @Column(name = "expiry_time")
  private Date ehsExpiryTime;

  @Column(name = "notification_status")
  private String notificationStatus;

  @Column(name = "comment")
  private String comment;

  @Column(name = "rev")
  private Integer rev;

  @OneToOne(cascade = { CascadeType.ALL }, targetEntity = RevInfo.class)
  @JoinColumn(name = "rev", insertable = false, updatable = false)
  private RevInfo revinfo;

  public PVehicleEHSChecklistAudit() {
  }

  public void setVehicleEHSAuditId(PVehicleEHSChecklistAuditId vehicleEHSAuditId) {
    this.vehicleEHSAuditId = vehicleEHSAuditId;
  }

  public PVehicleEHSChecklistAuditId getVehicleEHSAuditId() {
    return vehicleEHSAuditId;
  }

  public String getEhsStatus() {
    return ehsStatus;
  }

  public void setEhsStatus(String ehsStatus) {
    this.ehsStatus = ehsStatus;
  }

  public Date getEhsExpiryTime() {
    return ehsExpiryTime;
  }

  public void setEhsExpiryTime(Date ehsExpiryTime) {
    this.ehsExpiryTime = ehsExpiryTime;
  }

  public String getNotificationStatus() {
    return notificationStatus;
  }

  public void setNotificationStatus(String notificationStatus) {
    this.notificationStatus = notificationStatus;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public RevInfo getRevinfo() {
    return revinfo;
  }

  public void setRevinfo(RevInfo revinfo) {
    this.revinfo = revinfo;
  }

  public Integer getRev() {
    return rev;
  }

  public void setRev(Integer rev) {
    this.rev = rev;
  }

}
