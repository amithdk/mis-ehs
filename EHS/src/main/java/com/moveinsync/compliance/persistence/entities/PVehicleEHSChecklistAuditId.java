package com.moveinsync.compliance.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PVehicleEHSChecklistAuditId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 4687965648452369968L;

  @Column(name = "vehicle_id")
  private String vehicleId;

  @Column(name = "update_time")
  private Date updateTime;

  @Column(name = "business_unit_id")
  private String businessUnitId;

  @Column(name = "checklist_id")
  private String checklistId;

  public PVehicleEHSChecklistAuditId() {
  }

  public PVehicleEHSChecklistAuditId(String vehicleId, Date updateTime, String businessUnitId, String checklistId) {
    this.vehicleId = vehicleId;
    this.updateTime = updateTime;
    this.businessUnitId = businessUnitId;
    this.checklistId = checklistId;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  public String getBusinessUnitId() {
    return businessUnitId;
  }

  public String getChecklistId() {
    return checklistId;
  }

}
