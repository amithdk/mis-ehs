package com.moveinsync.compliance.persistence.entities;

import org.hibernate.envers.Audited;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * @author rajanish
 *
 */

@Audited
@Entity
@Table(name = "vehicle_ehs_checklist_status")
public class PVehicleEHSChecklistStatus {

  @EmbeddedId
  private PVehicleEHSChecklistStatusId id;

  @Column(name = "status")
  private String ehsStatus;

  @Column(name = "update_time")
  private Date ehsStatusUpdateTime;

  @Column(name = "expiry_time")
  private Date ehsExpiryTime;

  @Column(name = "notification_status")
  private String notificationStatus;

  @Column(name = "comment")
  private String comment;

  PVehicleEHSChecklistStatus() {
  }

  public PVehicleEHSChecklistStatus(String vehicleId, String businessUnitId, String checklistId) {
    this.id = new PVehicleEHSChecklistStatusId(vehicleId, businessUnitId, checklistId);
  }

  public PVehicleEHSChecklistStatusId getId() {
    return id;
  }

  public String getEhsStatus() {
    return ehsStatus;
  }

  public void setEhsStatus(String ehsStatus) {
    this.ehsStatus = ehsStatus;
  }

  public Date getEhsStatusUpdateTime() {
    return ehsStatusUpdateTime;
  }

  public void setEhsStatusUpdateTime(Date ehsStatusUpdateTime) {
    this.ehsStatusUpdateTime = ehsStatusUpdateTime;
  }

  public Date getEhsExpiryTime() {
    return ehsExpiryTime;
  }

  public void setEhsExpiryTime(Date ehsExpiryTime) {
    this.ehsExpiryTime = ehsExpiryTime;
  }

  public String getNotificationStatus() {
    return notificationStatus;
  }

  public void setNotificationStatus(String notificationStatus) {
    this.notificationStatus = notificationStatus;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getComment() {
    return comment;
  }

}
