package com.moveinsync.compliance.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PVehicleEHSChecklistStatusId implements Serializable {

  /**
   * @author rajanish
   * 
   */
  private static final long serialVersionUID = 4230105122235344706L;

  @Column(name = "vehicle_id")
  private String vehicleId;

  @Column(name = "business_unit_id")
  private String businessUnitId;

  @Column(name = "checklist_id")
  private String checklistId;

  PVehicleEHSChecklistStatusId() {
  }

  public PVehicleEHSChecklistStatusId(String vehicleId, String businessUnitId, String checklistId) {
    this.vehicleId = vehicleId;
    this.businessUnitId = businessUnitId;
    this.checklistId = checklistId;
  }

  public String getChecklistId() {
    return checklistId;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public String getBusinessUnitId() {
    return businessUnitId;
  }

}
