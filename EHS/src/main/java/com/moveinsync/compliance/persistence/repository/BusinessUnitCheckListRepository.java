package com.moveinsync.compliance.persistence.repository;

import com.moveinsync.compliance.persistence.entities.PBusinessUnitCheckList;
import com.moveinsync.compliance.persistence.entities.PBusinessUnitCheckListId;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BusinessUnitCheckListRepository
		extends JpaRepository<PBusinessUnitCheckList, PBusinessUnitCheckListId> {

	@Query("from PBusinessUnitCheckList where id.buid = ?1")
	List<PBusinessUnitCheckList> findByBusinessUnit(String businessUnit);
	
	@Query("from PBusinessUnitCheckList where id.checklistId = ?1 and business_unit_id = ?2")
	PBusinessUnitCheckList findByCheckListID(String checkList, String buId);
	
	@Query(nativeQuery=true, value = "select * from business_unit_checklists where business_unit_id = ?1 and ehs_audit_enabled = 'true'")
    List<PBusinessUnitCheckList> findByBusinessUnitAndEhsAuditEnabled(String businessUnit);
}