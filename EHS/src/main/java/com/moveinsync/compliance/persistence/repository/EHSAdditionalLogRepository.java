package com.moveinsync.compliance.persistence.repository;

import com.moveinsync.compliance.persistence.entities.PEHSAdditionalLog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface EHSAdditionalLogRepository extends JpaRepository<PEHSAdditionalLog, String>{
  
  @Query("from PEHSAdditionalLog  where businessUnit = ?1")
  public List<PEHSAdditionalLog> findByBuid(String businessUnit);
  
  @Query("from PEHSAdditionalLog where vehicleId = ?1")
  public PEHSAdditionalLog findByVehicleId(String vehicleId);
  
  @Query("from PEHSAdditionalLog where updateTime = ?1")
  public PEHSAdditionalLog findByUpdateTime(Date updateTime);

}
