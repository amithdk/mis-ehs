package com.moveinsync.compliance.persistence.repository;

import com.moveinsync.compliance.persistence.entities.EHSemailAlerts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EHSAlertNotificationRepository extends JpaRepository<EHSemailAlerts, String> {

	@Query("from EHSemailAlerts  where dailyDigest = ?1")
	public List<EHSemailAlerts> findByBuId(boolean flag);

	@Query("from EHSemailAlerts  where approachingExpiry = ?1")
	List<EHSemailAlerts> findEHSemailAlertsByApproachingExpiry(boolean flag);

	@Query("from EHSemailAlerts where buId = ?1")
	EHSemailAlerts findByBusinessUnitId(String buId);

}