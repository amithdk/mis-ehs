package com.moveinsync.compliance.persistence.repository;

import com.moveinsync.compliance.persistence.entities.PVehicleEHSChecklistAudit;
import com.moveinsync.compliance.persistence.entities.PVehicleEHSChecklistAuditId;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface EHSCheckAuditRepository extends JpaRepository<PVehicleEHSChecklistAudit, PVehicleEHSChecklistAuditId> {

  @Query("from PVehicleEHSChecklistAudit where businessUnitId = :businessUnit and vehicleEHSAuditId.vehicleId = :vehicleId and ehsStatusUpdateTime between :from and :upto")
  List<PVehicleEHSChecklistAudit> findEHSUpdatedVehicleByDateRange(@Param("businessUnit") String buid,
      @Param("vehicleId") String vehicleId, @Param(value = "from") Date from, @Param(value = "upto") Date upto);

  @Query("from PVehicleEHSChecklistAudit where businessUnitId = :businessUnit and ehsStatusUpdateTime between :from and :upto")
  List<PVehicleEHSChecklistAudit> findEHSUpdatedByDateRange(@Param("businessUnit") String buid,
      @Param(value = "from") Date from, @Param(value = "upto") Date upto);

  @Query("from PVehicleEHSChecklistAudit where vehicleEHSAuditId.businessUnitId = :businessUnit and vehicleEHSAuditId.vehicleId = :vehicleId and vehicleEHSAuditId.updateTime = :updateTime")
  List<PVehicleEHSChecklistAudit> findEHSUpdatedVehicleForTimeStamp(@Param("businessUnit") String buid,
      @Param("vehicleId") String vehicleId, @Param(value = "updateTime") Date updateTime);

  @Query("from PVehicleEHSChecklistAudit where vehicleEHSAuditId.businessUnitId = :businessUnit and vehicleEHSAuditId.updateTime = :updateTime")
  List<PVehicleEHSChecklistAudit> findEHSUpdatedForTimeStamp(@Param("businessUnit") String buid,
      @Param(value = "updateTime") Date updateTime);

}
