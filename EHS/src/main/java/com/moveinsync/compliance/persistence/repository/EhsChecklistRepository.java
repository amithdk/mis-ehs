package com.moveinsync.compliance.persistence.repository;

import com.moveinsync.compliance.persistence.entities.PEHSCheckList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface EhsChecklistRepository extends JpaRepository<PEHSCheckList, String> {
	@Query(nativeQuery = true, value ="select * from ehs_checklists where id in ?1")
	public List<PEHSCheckList> getEHSCheckListByCheckListName(Set<String> checkList);
	
	@Query(nativeQuery = true, value = "select * from  ehs_checklists where name in ?1")
	public List<PEHSCheckList> getEHSCheckListByCheckListName(List<String> checkList);
}
