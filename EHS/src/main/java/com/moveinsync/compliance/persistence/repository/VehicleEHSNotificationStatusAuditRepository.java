package com.moveinsync.compliance.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.moveinsync.compliance.ets.persistence.entities.VehicleEHSNotificationStatusAuditDO;
import com.moveinsync.compliance.ets.persistence.entities.VehicleEHSNotificationStatusAuditId;

import java.util.Date;
import java.util.List;

public interface VehicleEHSNotificationStatusAuditRepository extends
    JpaRepository<VehicleEHSNotificationStatusAuditDO, VehicleEHSNotificationStatusAuditId> {

  @Query("from VehicleEHSNotificationStatusAuditDO where ehsAuditId.businessUnitId = :businessUnit and ehsAuditId.vehicleId = :vehicleId and ehsAuditId.ehsStatusUpdateTime between :from and :upto and ehs_audited = :ehsAudited")
  List<VehicleEHSNotificationStatusAuditDO> findEHSUpdatedVehicleByDateRange(@Param("businessUnit") String buid,
      @Param("vehicleId") String vehicleId, @Param(value = "from") Date from, @Param(value = "upto") Date upto,
      @Param(value = "ehsAudited") boolean ehsAudited);
  
  @Query("from VehicleEHSNotificationStatusAuditDO where ehsAuditId.businessUnitId = :businessUnit and ehsAuditId.ehsStatusUpdateTime between :from and :upto")
  List<VehicleEHSNotificationStatusAuditDO> findEHSUpdatedByDateRange(@Param("businessUnit") String buid,
      @Param(value = "from") Date from, @Param(value = "upto") Date upto);

  @Query("from VehicleEHSNotificationStatusAuditDO where ehsAuditId.businessUnitId = :businessUnit and ehsAuditId.vehicleId = :vehicleId and ehsAuditId.ehsStatusUpdateTime = :updateTime")
  List<VehicleEHSNotificationStatusAuditDO> findEHSUpdatedVehicleForTimeStamp(@Param("businessUnit") String buid,
      @Param("vehicleId") String vehicleId, @Param(value = "updateTime") Date updateTime);

  @Query("from VehicleEHSNotificationStatusAuditDO where ehsAuditId.businessUnitId = :businessUnit and ehsAuditId.ehsStatusUpdateTime = :updateTime")
  List<VehicleEHSNotificationStatusAuditDO> findEHSUpdatedForTimeStamp(@Param("businessUnit") String buid,
      @Param(value = "updateTime") Date updateTime);
  
  @Query("from VehicleEHSNotificationStatusAuditDO where ehsAuditId.businessUnitId = :businessUnit and ehsAuditId.ehsStatusUpdateTime between :from and :upto and ehs_status != :criteria and ehs_audited = :ehsAudited")
  List<VehicleEHSNotificationStatusAuditDO> findEHSUpdatedByDateRangeExcept(@Param("businessUnit") String buid,
      @Param(value = "from") Date from, @Param(value = "upto") Date upto, @Param(value = "criteria") String criteria,
      @Param(value = "ehsAudited") boolean ehsAudited);
}
