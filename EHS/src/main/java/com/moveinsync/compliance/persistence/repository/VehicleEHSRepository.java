package com.moveinsync.compliance.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.moveinsync.compliance.persistence.entities.PVehicleEHSChecklistStatus;
import com.moveinsync.compliance.persistence.entities.PVehicleEHSChecklistStatusId;
import com.moveinsync.data.envers.repository.MISRevisionRepository;

public interface VehicleEHSRepository extends JpaRepository<PVehicleEHSChecklistStatus, PVehicleEHSChecklistStatusId>,
		MISRevisionRepository<PVehicleEHSChecklistStatus, PVehicleEHSChecklistStatusId, Integer> {

	@Query("from PVehicleEHSChecklistStatus where id.businessUnitId = ?1")
	List<PVehicleEHSChecklistStatus> findByBuid(String buid);

	@Query("from PVehicleEHSChecklistStatus where id.vehicleId = ?1")
	List<PVehicleEHSChecklistStatus> findByVehicleId(String vehicleId);

	@Query("from PVehicleEHSChecklistStatus as pvcs where pvcs.id.vehicleId = :vehicleId  and pvcs.id.businessUnitId = :businessUnitId ORDER BY pvcs.ehsStatusUpdateTime DESC")
	List<PVehicleEHSChecklistStatus> findByVehicleIdBusinessUnitId(@Param("businessUnitId") String buID,
			@Param("vehicleId") String vehicleID);

}