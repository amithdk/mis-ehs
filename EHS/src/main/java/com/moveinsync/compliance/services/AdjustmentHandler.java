package com.moveinsync.compliance.services;

import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.models.ChecklistPenaltyDTO;

public class AdjustmentHandler implements Runnable {

  private AdjustmentService adjustmentService;

  private String businessUnit;

  private VehicleEHSChecklistVO failedCheckVO;

  public AdjustmentHandler(AdjustmentService adjustmentService, String businessUnit,
      VehicleEHSChecklistVO failedCheckVO) {
    this.adjustmentService = adjustmentService;
    this.businessUnit = businessUnit;
    this.failedCheckVO = failedCheckVO;
  }

  @Override
  public void run() {
    execute();
  }

  private void execute() {
    ChecklistPenaltyDTO checklistPenaltyDTO = adjustmentService.prepareAdjustmentDTO(failedCheckVO, businessUnit);
    adjustmentService.sendAdjustment(checklistPenaltyDTO, businessUnit);
  }

}
