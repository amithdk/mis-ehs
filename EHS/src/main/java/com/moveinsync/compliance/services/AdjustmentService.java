package com.moveinsync.compliance.services;

import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.models.ChecklistPenaltyDTO;

public interface AdjustmentService {
  
  public ChecklistPenaltyDTO prepareAdjustmentDTO(VehicleEHSChecklistVO failedCheckVO,String buId);
  
  public void sendAdjustment(ChecklistPenaltyDTO checklistPenaltyDTO,String buid);

}
