package com.moveinsync.compliance.services;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.ets.model.Vehicle;
import com.moveinsync.compliance.ets.services.service.VmsService;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleAdjustmentVO;
import com.moveinsync.compliance.models.ChecklistPenaltyDTO;
import com.moveinsync.compliance.types.VehicleEHSStatus;
import com.moveinsync.compliance.utils.ComplianceUtils;

/**
 * @author rajanish
 * @date 1 August 2018
 *
 */

@Service
public class AdjustmentServiceImpl implements AdjustmentService {

  @Autowired
  VmsService vms;

  @Autowired
  BusinessUnitCheckListService buChecklistService;

  static final Logger logger = LoggerFactory.getLogger(AdjustmentService.class);

  @Override
  public ChecklistPenaltyDTO prepareAdjustmentDTO(VehicleEHSChecklistVO failedCheckVO, String buId) {
    ChecklistPenaltyDTO checklistPenaltyDTO = new ChecklistPenaltyDTO();
    Vehicle vehicle = vms.getActiveVehicle(buId, failedCheckVO.getRegistrationNumber());
    checklistPenaltyDTO.setNativeCabId(String.valueOf(vehicle.getNativeVehicleId()));
    List<String> adjustments = Lists.newArrayList();
    Map<String, BUVehicleAdjustmentVO> checklistAdjustmentmap = prepareChecklistMap(buChecklistService
        .getPenaltyForBU(buId));
    Map<String, String> failedChecklists = failedCheckVO.getChecklistStatusMap();
    for (Map.Entry<String, String> entry : failedChecklists.entrySet()) {
      if (entry.getValue().equals(VehicleEHSStatus.FAILED.name())) {
        continue;
      }

      if (checklistAdjustmentmap.containsKey(entry.getKey())) {
        adjustments.add(checklistAdjustmentmap.get(entry.getKey()).getAdjustment());
      }
    }
    checklistPenaltyDTO.setAdjusmentTypeIds(adjustments);
    return checklistPenaltyDTO;
  }

  @Override
  public void sendAdjustment(ChecklistPenaltyDTO penality, String businessUnitId) {
    if (CollectionUtils.isEmpty(penality.getAdjusmentTypeIds())) {
      logger.info("No Adustments found for native cab id {} of buId {}", penality.getNativeCabId(), businessUnitId);
      return;
    }
    RestTemplate restTemplate = new RestTemplate();
    String url = ComplianceUtils.getUrlFromBU(businessUnitId);
    url += "/ets/apis/penalty";
    logger.info("Adjustment creation url {}", url);
    try {
      HttpHeaders headers = getHeaders();
      HttpEntity<ChecklistPenaltyDTO> requestBody = new HttpEntity<ChecklistPenaltyDTO>(penality, headers);
      restTemplate.postForLocation(url, requestBody);
      logger.debug("Adjustment created Successfully  for buId: {} of cab Id: {} ", businessUnitId,
          penality.getNativeCabId());
    } catch (Exception e) {
      logger.info("Failed to create penalty for buId: {}  of cab Id: {} ", businessUnitId, penality.getNativeCabId());
      logger.info("Penalty Exception {} ", e.getMessage());
    }
  }
  
  private HttpHeaders getHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.set("x-mis-token", "Rf2zAClKVcvlYGCz1S2h");
    headers.setContentType(MediaType.APPLICATION_JSON);
    return headers;
  }
  
  private Map<String, BUVehicleAdjustmentVO> prepareChecklistMap(List<BUVehicleAdjustmentVO> buAdjustmentVOs) {
    Map<String, BUVehicleAdjustmentVO> buMap = Maps.newHashMap();
    for (BUVehicleAdjustmentVO buVo : buAdjustmentVOs) {
      buMap.put(buVo.getChecklistName(), buVo);
    }
    return buMap;
  }

}
