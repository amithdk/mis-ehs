package com.moveinsync.compliance.services;

import com.moveinsync.compliance.dto.BusinessUnitCheckListDTO;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleAdjustmentVO;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleChecklistVO;
import com.moveinsync.compliance.ets.web.viewobject.BusinessUnitChecklistVO;

import java.util.List;

public interface BusinessUnitCheckListService {

	  List<BusinessUnitCheckListDTO> getCheckListsForBuid(String buid);
	  
	  public List<BusinessUnitCheckListDTO> buChecklistEntryGenerator(String businessUnit,List<BUVehicleChecklistVO> checkListVOs) ;

	  List<BusinessUnitChecklistVO> getChecklistVOForBU(String buid); 
	  
	  public void addPenaltyForBU(String businessUnit,List<BUVehicleAdjustmentVO> buVehicleAdjustmentVOs);
	  
	  public List<BUVehicleAdjustmentVO> getPenaltyForBU(String businessUnit);

	  List<BusinessUnitCheckListDTO> getEhsAuditEnabledCheckListsForBuid(String buid);
}
