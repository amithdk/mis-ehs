package com.moveinsync.compliance.services;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.moveinsync.compliance.dto.BusinessUnitCheckListDTO;
import com.moveinsync.compliance.dto.EHSChecklistDTO;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleAdjustmentVO;
import com.moveinsync.compliance.ets.web.viewobject.BUVehicleChecklistVO;
import com.moveinsync.compliance.ets.web.viewobject.BusinessUnitChecklistVO;
import com.moveinsync.compliance.persistence.entities.PBusinessUnitCheckList;
import com.moveinsync.compliance.persistence.repository.BusinessUnitCheckListRepository;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class BusinessUnitCheckListServiceImpl implements BusinessUnitCheckListService {

  @Autowired
  EHSChecklistService ehsCheckListService;

  @Autowired
  BusinessUnitCheckListRepository buCheckListRepo;

  @Override
  public List<BusinessUnitCheckListDTO> getCheckListsForBuid(String buid) {
    List<BusinessUnitCheckListDTO> businessUnitCheckListDTOs = Lists.newArrayList();
    List<PBusinessUnitCheckList> pCheckLists = buCheckListRepo.findByBusinessUnit(buid);
    for (PBusinessUnitCheckList pCheckList : pCheckLists) {
      businessUnitCheckListDTOs.add(convert(pCheckList));
    }
    return businessUnitCheckListDTOs;
  }
  
  @Override
  public List<BusinessUnitCheckListDTO> getEhsAuditEnabledCheckListsForBuid(String buid) {
    List<BusinessUnitCheckListDTO> businessUnitCheckListDTOs = Lists.newArrayList();
    List<PBusinessUnitCheckList> pCheckLists = buCheckListRepo.findByBusinessUnitAndEhsAuditEnabled(buid);
    for(PBusinessUnitCheckList pCheckList : pCheckLists) {
      businessUnitCheckListDTOs.add(convert(pCheckList));
    }
    return businessUnitCheckListDTOs;
  }

  @Override
  public List<BusinessUnitChecklistVO> getChecklistVOForBU(String buid) {
    List<BusinessUnitChecklistVO> buChecklistVOs = Lists.newArrayList();
    List<BusinessUnitCheckListDTO> businessUnitCheckListDTOs = getEhsAuditEnabledCheckListsForBuid(buid);
    List<String> checkListIds = Lists.newArrayList();
    for (BusinessUnitCheckListDTO buDTO : businessUnitCheckListDTOs) {
      checkListIds.add(buDTO.getChecklistId());
    }
    Map<String, EHSChecklistDTO> checklistDTOs = ehsCheckListService.getChecklistsById(checkListIds);
    for (BusinessUnitCheckListDTO buDto : businessUnitCheckListDTOs) {
      BusinessUnitChecklistVO buVO = new BusinessUnitChecklistVO();
      EHSChecklistDTO checklistDTO = checklistDTOs.get(buDto.getChecklistId());
      buVO.setChecklistId(buDto.getChecklistId());
      buVO.setHintText(checklistDTO.getHintText());
      buVO.setIconPath(checklistDTO.getIconPath());
      buVO.setMandatory(buDto.isMandatory());
      buVO.setName(checklistDTO.getName());
      buVO.setSequence(buDto.getSequence());
      buVO.setType(checklistDTO.getType());
      buChecklistVOs.add(buVO);
    }
    return buChecklistVOs;

  }

  @Override
  public List<BusinessUnitCheckListDTO> buChecklistEntryGenerator(String businessUnit,
      List<BUVehicleChecklistVO> buVehcileChecklistVOS) {
    List<PBusinessUnitCheckList> pBUCheckLists = Lists.newArrayList();
    Map<String, EHSChecklistDTO> checklistMap = ehsCheckListService
        .getChecklistsByName(Collections.<String> emptyList());
    for (BUVehicleChecklistVO buVO : buVehcileChecklistVOS) {
      EHSChecklistDTO dtoForVO = checklistMap.get(buVO.getCheckListName());
      if (dtoForVO == null) {
        continue;
      }
      BusinessUnitCheckListDTO businessUnitCheckListDTO = new BusinessUnitCheckListDTO(businessUnit, dtoForVO.getId());
      businessUnitCheckListDTO.setAdjustment(buVO.getAdjustment());
      businessUnitCheckListDTO.setMandatory(buVO.isMandatory());
      businessUnitCheckListDTO.setSequence(buVO.getSequence());
      pBUCheckLists.add(convert(businessUnitCheckListDTO));
    }
    pBUCheckLists = buCheckListRepo.saveAll(pBUCheckLists);
    List<BusinessUnitCheckListDTO> buCheckListDTOs = Lists.newArrayList();
    for (PBusinessUnitCheckList pBUCheckList : pBUCheckLists) {
      buCheckListDTOs.add(convert(pBUCheckList));
    }
    return buCheckListDTOs;
  }

  @Override
  public void addPenaltyForBU(String businessUnit, List<BUVehicleAdjustmentVO> buVehicleAdjustmentVOs) {
    List<PBusinessUnitCheckList> pBusinessUnitCheckLists = getPenaltyFromVO(businessUnit, buVehicleAdjustmentVOs);
    buCheckListRepo.saveAll(pBusinessUnitCheckLists);
  }
  
  @Override
  public List<BUVehicleAdjustmentVO> getPenaltyForBU(String businessUnit) {
    List<PBusinessUnitCheckList> pBusinessUnitCheckLists = buCheckListRepo.findByBusinessUnit(businessUnit);
    List<BUVehicleAdjustmentVO> buAdjustmentVOs = Lists.newArrayList();
    for (PBusinessUnitCheckList checklist : pBusinessUnitCheckLists) {
      if (StringUtils.isNotBlank(checklist.getAdjustment())) {
        BUVehicleAdjustmentVO buVo = new BUVehicleAdjustmentVO();
        buVo.setAdjustment(checklist.getAdjustment());
        buVo.setBuid(businessUnit);
        buVo.setChecklistName(checklist.getId().getChecklistId());
        buAdjustmentVOs.add(buVo);
      }
    }
    return buAdjustmentVOs;
  }

  private List<PBusinessUnitCheckList> getPenaltyFromVO(String businessUnit,
      List<BUVehicleAdjustmentVO> buVehicleAdjustmentVOs) {
    Map<String, EHSChecklistDTO> checklistNameMap = ehsCheckListService
        .getChecklistsByName(Collections.<String> emptyList());
    List<BusinessUnitCheckListDTO> businessUnitCheckListDTOs = getCheckListsForBuid(businessUnit);
    Map<String, BusinessUnitCheckListDTO> checklistIdBuDtoMap = convert(businessUnitCheckListDTOs);
    List<PBusinessUnitCheckList> pBusinessUnitCheckLists = Lists.newArrayList();
    
    for (BUVehicleAdjustmentVO buVo : buVehicleAdjustmentVOs) {
      if (checklistNameMap.containsKey(buVo.getChecklistName())) {
        EHSChecklistDTO checklistDTO = checklistNameMap.get(buVo.getChecklistName());
        BusinessUnitCheckListDTO buCheckListDTO = checklistIdBuDtoMap.get(checklistDTO.getId());
        PBusinessUnitCheckList pCheckList = new PBusinessUnitCheckList(businessUnit, checklistDTO.getId());
        pCheckList.setAdjustment(buVo.getAdjustment());
        pCheckList.setMandatory(buCheckListDTO.isMandatory());
        pCheckList.setSequence(buCheckListDTO.getSequence());
        pBusinessUnitCheckLists.add(pCheckList);
      }
    }
    return pBusinessUnitCheckLists;
  }

  private Map<String, BusinessUnitCheckListDTO> convert(List<BusinessUnitCheckListDTO> buDTOs) {
    Map<String, BusinessUnitCheckListDTO> checklistIdBuDTOMap = Maps.newHashMap();
    for (BusinessUnitCheckListDTO buDto : buDTOs) {
      checklistIdBuDTOMap.put(buDto.getChecklistId(), buDto);
    }
    return checklistIdBuDTOMap;
  }

  private BusinessUnitCheckListDTO convert(PBusinessUnitCheckList pCheckList) {
    BusinessUnitCheckListDTO buCheckListDTO = new BusinessUnitCheckListDTO(pCheckList.getId().getBuid(),
        pCheckList.getId().getChecklistId());
    buCheckListDTO.setAdjustment(pCheckList.getAdjustment());
    buCheckListDTO.setMandatory(pCheckList.isMandatory());
    buCheckListDTO.setSequence(pCheckList.getSequence());
    return buCheckListDTO;
  }

  private PBusinessUnitCheckList convert(BusinessUnitCheckListDTO buCheckListDTO) {
    PBusinessUnitCheckList pBusinessUnitCheckList = new PBusinessUnitCheckList(buCheckListDTO.getBuisinessUnit(),
        buCheckListDTO.getChecklistId());
    pBusinessUnitCheckList.setAdjustment(buCheckListDTO.getAdjustment());
    pBusinessUnitCheckList.setMandatory(buCheckListDTO.isMandatory());
    pBusinessUnitCheckList.setSequence(buCheckListDTO.getSequence());
    pBusinessUnitCheckList.setEhsAuditEnabled(true);
    return pBusinessUnitCheckList;
  }

}
