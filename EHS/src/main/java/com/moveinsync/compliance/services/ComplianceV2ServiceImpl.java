package com.moveinsync.compliance.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.moveinsync.compliance.models.Alerts;
import com.moveinsync.compliance.models.VehicleEHSLastCheckStatusDTO;

@Service
public class ComplianceV2ServiceImpl {
	
	@Value(value="${compliance.domain}") String baseUrl;
	@Autowired RestTemplate template;
	public boolean enableEhs(String buid) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+buid;
		ResponseEntity<Boolean> postForEntity = template.postForEntity(restUrl, null, Boolean.class);
		return postForEntity.getBody().booleanValue();
	}
	
	public String getVehicleComplianceStatus(String buid, String vehicleId) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+buid+ "/vehicles/" + vehicleId + "/status" ;
		ResponseEntity<String> postForEntity = template.getForEntity(restUrl, String.class);
		return postForEntity.getBody();
	}
	
	public List<VehicleEHSLastCheckStatusDTO> getVehicles(String buid) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+buid+"/vehicles";
		ResponseEntity<List<VehicleEHSLastCheckStatusDTO>> getForEntity = template.exchange(restUrl,HttpMethod.GET,null, 
					new ParameterizedTypeReference<List<VehicleEHSLastCheckStatusDTO>>() {});
		return getForEntity.getBody();
	}
	
	public String getEhsStatus(String buid, String registrationNumber) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+buid+ "/vehicles/" + registrationNumber + "/ehsStatus" ;
		ResponseEntity<String> postForEntity = template.getForEntity(restUrl, String.class);
		return postForEntity.getBody(); 
	}

	public void updateEhsStatus(String buid, String vehicleId, String registrationNumber, String status) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+buid + "/vehicles/" + vehicleId + "/" + registrationNumber + "/" + status;
		template.getForEntity(restUrl, String.class);
	}

	public Alerts getVehicleAlerts(String businessUnit) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+businessUnit + "/alerts";
		return template.getForEntity(restUrl, Alerts.class).getBody();
	}

	public List<String> getVehicleIdsForBu(String businessUnit) {
		String restUrl = baseUrl + "/rest/v2.0/compliance/"+businessUnit;
		return template.exchange(restUrl,HttpMethod.GET,null, new ParameterizedTypeReference<List<String>>() {}).getBody();
	}
}
