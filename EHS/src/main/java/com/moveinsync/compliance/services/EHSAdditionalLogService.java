package com.moveinsync.compliance.services;

import com.moveinsync.compliance.dto.EHSAdditionalLogDTO;

import java.util.Date;
import java.util.List;

public interface EHSAdditionalLogService {
  
  public List<EHSAdditionalLogDTO> getByBusinessUnit(String businessUnit);
  
  public EHSAdditionalLogDTO getByVehicleId(String vehicleId);
  
  public void save(List<EHSAdditionalLogDTO> ehsAdditionalLogDTOs);
  
  public EHSAdditionalLogDTO getByUpdateTime(Date updateTime);

}
