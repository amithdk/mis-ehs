package com.moveinsync.compliance.services;

import com.google.common.collect.Lists;
import com.moveinsync.compliance.dto.EHSAdditionalLogDTO;
import com.moveinsync.compliance.persistence.entities.PEHSAdditionalLog;
import com.moveinsync.compliance.persistence.repository.EHSAdditionalLogRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author rajanish
 * @since 4 August 2018
 */

@Service
public class EHSAdditionalLogServiceImpl implements EHSAdditionalLogService {

  @Autowired
  EHSAdditionalLogRepository ehsLogRepo;

  @Override
  public List<EHSAdditionalLogDTO> getByBusinessUnit(String businessUnit) {
    List<EHSAdditionalLogDTO> ehsAdditionalLogDTOs = Lists.newArrayList();
    List<PEHSAdditionalLog> pEhAdditionalLogs = ehsLogRepo.findByBuid(businessUnit);
    for (PEHSAdditionalLog pLog : pEhAdditionalLogs) {
      ehsAdditionalLogDTOs.add(convert(pLog));
    }
    return ehsAdditionalLogDTOs;
  }

  @Override
  public EHSAdditionalLogDTO getByVehicleId(String vehicleId) {
    PEHSAdditionalLog pEhAdditionalLogs = ehsLogRepo.findByVehicleId(vehicleId);
    return convert(pEhAdditionalLogs);
  }

  @Override
  public EHSAdditionalLogDTO getByUpdateTime(Date updateTime) {
    PEHSAdditionalLog pehsAdditionalLog = ehsLogRepo.findByUpdateTime(updateTime);
    return convert(pehsAdditionalLog);
  }

  @Override
  public void save(List<EHSAdditionalLogDTO> ehsAdditionalLogDTOs) {
    List<PEHSAdditionalLog> pLogs = Lists.newArrayList();
    for (EHSAdditionalLogDTO eDto : ehsAdditionalLogDTOs) {
      pLogs.add(convert(eDto));
    }
    ehsLogRepo.saveAll(pLogs);
  }

  private EHSAdditionalLogDTO convert(PEHSAdditionalLog pLog) {
    EHSAdditionalLogDTO eDto = new EHSAdditionalLogDTO();
    if (pLog == null)
      return eDto;
    eDto.setBusinessUnit(pLog.getBusinessUnit());
    eDto.setDriverName(pLog.getDriverName());
    eDto.setFacility(pLog.getFacility());
    eDto.setKmsTravelled(pLog.getKmsTravelled());
    eDto.setVehicleId(pLog.getVehicleId());
    eDto.setUpdateTime(pLog.getUpdateTime());
    return eDto;
  }

  private PEHSAdditionalLog convert(EHSAdditionalLogDTO eDto) {
    PEHSAdditionalLog pLog = new PEHSAdditionalLog();
    pLog.setDriverName(eDto.getDriverName());
    pLog.setFacility(eDto.getFacility());
    pLog.setBusinessUnit(eDto.getBusinessUnit());
    pLog.setVehicleId(eDto.getVehicleId());
    pLog.setUpdateTime(eDto.getUpdateTime());
    pLog.setKmsTravelled(eDto.getKmsTravelled());
    pLog.setUpdateTime(eDto.getUpdateTime());
    return pLog;
  }

}
