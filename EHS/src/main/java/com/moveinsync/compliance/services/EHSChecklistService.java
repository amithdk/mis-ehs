package com.moveinsync.compliance.services;

import java.util.List;
import java.util.Map;

import com.moveinsync.compliance.dto.EHSChecklistDTO;
import com.moveinsync.compliance.ets.web.viewobject.EHSCheckListVO;

public interface EHSChecklistService {

    public List<EHSChecklistDTO> getAllChecklists();
    
    public Map<String,EHSChecklistDTO> getChecklistsById(List<String> checklistIdList);
    
    public Map<String, EHSChecklistDTO> getChecklistsByName(List<String> checklistNameList);
    
    public List<EHSCheckListVO> ehsChecklistsGenerator(List<EHSCheckListVO> vehicleEHSCheckListVOs);

}