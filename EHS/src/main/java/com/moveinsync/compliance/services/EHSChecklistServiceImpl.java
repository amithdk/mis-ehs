package com.moveinsync.compliance.services;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.moveinsync.compliance.dto.EHSChecklistDTO;
import com.moveinsync.compliance.ets.web.viewobject.EHSCheckListVO;
import com.moveinsync.compliance.persistence.entities.PEHSCheckList;
import com.moveinsync.compliance.persistence.repository.EhsChecklistRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author rajanish
 * @since 21 March 2018
 */

@Service
public class EHSChecklistServiceImpl implements EHSChecklistService {

  @Autowired
  EhsChecklistRepository ehsRepo;

  @Override
  public List<EHSCheckListVO> ehsChecklistsGenerator(List<EHSCheckListVO> vehicleEHSCheckListVOs) {
    List<PEHSCheckList> pCheckLists = ehsRepo.findAll();
    Map<String, EHSChecklistDTO> ehsChecklistMap = getChecklistsByName(Collections.<String> emptyList());
    List<EHSCheckListVO> ehsChecklistVOs = Lists.newArrayList();
    if (vehicleEHSCheckListVOs.isEmpty()) {
      for (PEHSCheckList pehsCheckList : pCheckLists) {
        ehsChecklistVOs.add(convertToVO(convert(pehsCheckList)));
      }
      return ehsChecklistVOs;
    }
    for (EHSCheckListVO vehicleEHSCheckListVO : vehicleEHSCheckListVOs) {
      if (ehsChecklistMap.containsKey(vehicleEHSCheckListVO.getCheckListName())) {
        EHSChecklistDTO existingChecklistDTO = ehsChecklistMap.get(vehicleEHSCheckListVO.getCheckListName());
        PEHSCheckList updatedChecklist = getUpdatedChecklist(existingChecklistDTO, vehicleEHSCheckListVO);
        ehsRepo.save(updatedChecklist);
      } else {
        ehsRepo.save(convert(convert(vehicleEHSCheckListVO)));
      }
    }
    List<EHSChecklistDTO> ehsChecklistDTOs = getAllChecklists();
    for (EHSChecklistDTO ehsChecklistDTO : ehsChecklistDTOs) {
      ehsChecklistVOs.add(convertToVO(ehsChecklistDTO));
    }
    return ehsChecklistVOs;
  }

  @Override
  public List<EHSChecklistDTO> getAllChecklists() {
    List<EHSChecklistDTO> checklistDTOs = Lists.newArrayList();
    List<PEHSCheckList> pehsCheckLists = ehsRepo.findAll();
    for (PEHSCheckList pehsCheckList : pehsCheckLists) {
      checklistDTOs.add(convert(pehsCheckList));
    }
    return checklistDTOs;
  }

  @Override
  public Map<String, EHSChecklistDTO> getChecklistsById(List<String> checklistIdList) {
    Map<String, EHSChecklistDTO> ehsChecklistDTOs = Maps.newHashMap();
    List<PEHSCheckList> pehsCheckLists = Lists.newArrayList();
    if (checklistIdList.isEmpty()) {
      pehsCheckLists = ehsRepo.findAll();
    } else {
      pehsCheckLists = ehsRepo.findAllById(checklistIdList);
    }
    for (PEHSCheckList pehsCheckList : pehsCheckLists) {
      ehsChecklistDTOs.put(pehsCheckList.getId(), convert(pehsCheckList));
    }
    return ehsChecklistDTOs;
  }

  @Override
  public Map<String, EHSChecklistDTO> getChecklistsByName(List<String> checklistNameList) {
    Map<String, EHSChecklistDTO> ehsChecklistDTOs = Maps.newHashMap();
    List<PEHSCheckList> pehsCheckLists = Lists.newArrayList();
    if (checklistNameList.isEmpty()) {
      pehsCheckLists = ehsRepo.findAll();
    } else {
      pehsCheckLists = ehsRepo.getEHSCheckListByCheckListName(checklistNameList);
    }
    for (PEHSCheckList pehsCheckList : pehsCheckLists) {
      ehsChecklistDTOs.put(pehsCheckList.getName(), convert(pehsCheckList));
    }
    return ehsChecklistDTOs;
  }

  private PEHSCheckList getUpdatedChecklist(EHSChecklistDTO existingChecklistDTO, EHSCheckListVO checkListVO) {
    PEHSCheckList pehsCheckList = new PEHSCheckList(checkListVO.getCheckListName());
    pehsCheckList.setId(existingChecklistDTO.getId());
    pehsCheckList.setHintText(checkListVO.getHintText());
    pehsCheckList.setIconPath(checkListVO.getIconPath());
    pehsCheckList.setType(checkListVO.getType());
    return pehsCheckList;
  }

  private EHSChecklistDTO convert(PEHSCheckList pehsCheckList) {
    EHSChecklistDTO ehsChecklistDTO = new EHSChecklistDTO();
    ehsChecklistDTO.setIconPath(pehsCheckList.getIconPath());
    ehsChecklistDTO.setId(pehsCheckList.getId());
    ehsChecklistDTO.setName(pehsCheckList.getName());
    ehsChecklistDTO.setType(pehsCheckList.getType());
    ehsChecklistDTO.setHintText(pehsCheckList.getHintText());
    return ehsChecklistDTO;
  }

  private PEHSCheckList convert(EHSChecklistDTO ehsChecklistDTO) {
    PEHSCheckList pehsCheckList = new PEHSCheckList(ehsChecklistDTO.getName());
    pehsCheckList.setHintText(ehsChecklistDTO.getHintText());
    pehsCheckList.setIconPath(ehsChecklistDTO.getIconPath());
    pehsCheckList.setType(ehsChecklistDTO.getType());
    return pehsCheckList;
  }

  private EHSChecklistDTO convert(EHSCheckListVO vehVo) {
    EHSChecklistDTO checklistDTO = new EHSChecklistDTO();
    checklistDTO.setName(vehVo.getCheckListName());
    checklistDTO.setHintText(vehVo.getHintText());
    checklistDTO.setIconPath(vehVo.getIconPath());
    checklistDTO.setType(vehVo.getType());
    return checklistDTO;
  }

  private EHSCheckListVO convertToVO(EHSChecklistDTO checklistDTO) {
    EHSCheckListVO checklistVO = new EHSCheckListVO();
    checklistVO.setCheckListName(checklistDTO.getName());
    checklistVO.setHintText(checklistDTO.getHintText());
    checklistVO.setIconPath(checklistDTO.getIconPath());
    checklistVO.setType(checklistDTO.getType());
    return checklistVO;
  }

}
