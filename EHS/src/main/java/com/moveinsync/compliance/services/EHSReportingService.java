package com.moveinsync.compliance.services;

import com.moveinsync.compliance.dto.EHSAuditReportDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;

import java.util.Date;
import java.util.List;

public interface EHSReportingService {

  public List<EHSAuditReportDTO> getVehicleEHSAuditReport(String businessUnit, String vehicleID, Date from,
      Date upto);

  public List<EHSAuditReportDTO> getEHSAuditReport(String businessUnit, Date from, Date upto);
  public List<EHSAuditReportDTO> getEHSAuditReportFOrApproachingExpiry(String businessUnit, Date from, Date upto);

  public EHSAuditReportDTO processFailedEHSnotification(VehicleEHSChecklistVO ehsStatusResponseDTO, String businessUnit,
      String email,String comment);
  
  public void sendFailedEHSNotification(VehicleEHSChecklistVO ehsStatusResponseVO, EHSAuditReportDTO ehsAuditReportDTO,String businessUnit,String[] to,String[] replyTo);
  
}
