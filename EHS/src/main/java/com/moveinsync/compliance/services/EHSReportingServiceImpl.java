package com.moveinsync.compliance.services;

import com.google.common.collect.Lists;
import com.moveinsync.compliance.dto.EHSAdditionalLogDTO;
import com.moveinsync.compliance.dto.EHSAuditReportDTO;
import com.moveinsync.compliance.dto.EHSChecklistDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.ets.model.Vehicle;
import com.moveinsync.compliance.ets.persistence.entities.VehicleEHSNotificationStatusAuditDO;
import com.moveinsync.compliance.ets.persistence.repository.VehicleEHSNotificationRepository;
import com.moveinsync.compliance.ets.services.email.CommunicationService;
import com.moveinsync.compliance.ets.services.email.EmailTemplate;
import com.moveinsync.compliance.ets.services.service.VmsService;
import com.moveinsync.compliance.ets.web.util.EmailTemplateConstants;
import com.moveinsync.compliance.ets.web.util.WebConstants;
import com.moveinsync.compliance.persistence.entities.PVehicleEHSChecklistAudit;
import com.moveinsync.compliance.persistence.repository.EHSCheckAuditRepository;
import com.moveinsync.compliance.persistence.repository.VehicleEHSNotificationStatusAuditRepository;
import com.moveinsync.compliance.types.VehicleEHSStatus;
import com.moveinsync.compliance.utils.ComplianceUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author rajanish
 * @since 26 April 2018
 *
 */

@Service
public class EHSReportingServiceImpl implements EHSReportingService {

  @Autowired
  EHSCheckAuditRepository ehsAuditRepository;

  @Autowired
  VehicleEHSNotificationStatusAuditRepository ehsNotAuditRepository;

  @Autowired
  VehicleEHSNotificationRepository vNotRepo;

  @Autowired
  VmsService vms;

  @Autowired
  EHSChecklistService ehsChecklistService;

  @Autowired
  EHSAdditionalLogService ehLogService;

  @Autowired
  CommunicationService communicationService;

  SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

  static final Logger logger = LoggerFactory.getLogger(EHSReportingService.class);

  @Override
  public List<EHSAuditReportDTO> getVehicleEHSAuditReport(String businessUnit, String vehicleID, Date from, Date upto) {
    List<VehicleEHSNotificationStatusAuditDO> pNotificatonAudits = ehsNotAuditRepository
        .findEHSUpdatedVehicleByDateRange(businessUnit, vehicleID, from, upto, true);
    List<EHSAuditReportDTO> ehsAudits = Lists.newArrayList();
    Map<String, EHSChecklistDTO> checklistsMap = ehsChecklistService
        .getChecklistsById(Collections.<String> emptyList());

    for (VehicleEHSNotificationStatusAuditDO pNotificatonAudit : pNotificatonAudits) { // for each notification audits
      List<PVehicleEHSChecklistAudit> pvehicleEHSChecklistAudits = ehsAuditRepository.findEHSUpdatedVehicleForTimeStamp(
          businessUnit, pNotificatonAudit.getEhsAuditId().getVehicleId(),
          pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime());
      EHSAdditionalLogDTO ehsAdditionalLogDTO = ehLogService
          .getByUpdateTime(pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime());
      EHSAuditReportDTO ehsAuditDto = new EHSAuditReportDTO();
      ehsAuditDto.setAuditBy(pNotificatonAudit.getRevinfo().getMisUser());
      ehsAuditDto.setChangeTime(sdf.format(pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime()));
      ehsAuditDto.setComment(pNotificatonAudit.getComment());
      ehsAuditDto.setOverallStatus(pNotificatonAudit.getEhsStatus());
      ehsAuditDto.setFieldsRejected(getFailedChecklists(pvehicleEHSChecklistAudits, checklistsMap));
      ehsAuditDto.setDriver(ehsAdditionalLogDTO.getDriverName());
      ehsAuditDto.setKmsTravelled(ehsAdditionalLogDTO.getKmsTravelled());
      ehsAuditDto.setOffice(ehsAdditionalLogDTO.getFacility());
      ehsAudits.add(ehsAuditDto);
    }
    return ehsAudits;
  }

  @Override
  public List<EHSAuditReportDTO> getEHSAuditReport(String businessUnit, Date from, Date upto) {

    List<VehicleEHSNotificationStatusAuditDO> pNotificatonAudits = ehsNotAuditRepository
        .findEHSUpdatedByDateRangeExcept(businessUnit, from, upto, VehicleEHSStatus.APPROACHING_EXPIRY.name(), true);
    List<EHSAuditReportDTO> ehsAudits = Lists.newArrayList();
    Map<String, Vehicle> vehiclesMap = vms.getVehiclesMap(businessUnit);
    Map<String, EHSChecklistDTO> checklistsMap = ehsChecklistService
        .getChecklistsById(Collections.<String> emptyList());

    for (VehicleEHSNotificationStatusAuditDO pNotificatonAudit : pNotificatonAudits) {
      String user = pNotificatonAudit.getRevinfo() != null
          && StringUtils.isNotBlank(pNotificatonAudit.getRevinfo().getMisUser()) ? pNotificatonAudit.getRevinfo()
          .getMisUser() : StringUtils.EMPTY;
      if (user.equals("System") && !pNotificatonAudit.getEhsStatus().equals(VehicleEHSStatus.EXPIRED.name())) {
        continue;
      }
      Vehicle vehicle = vehiclesMap.get(pNotificatonAudit.getEhsAuditId().getVehicleId());
      List<PVehicleEHSChecklistAudit> pvehicleEHSChecklistAudits = ehsAuditRepository.findEHSUpdatedVehicleForTimeStamp(
          businessUnit, pNotificatonAudit.getEhsAuditId().getVehicleId(),
          pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime());
      EHSAdditionalLogDTO ehsAdditionalLogDTO = ehLogService
          .getByUpdateTime(pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime());
      EHSAuditReportDTO ehsAuditDto = new EHSAuditReportDTO();
      ehsAuditDto.setAuditBy(user);
      ehsAuditDto.setChangeTime(sdf.format(pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime()));
      ehsAuditDto.setComment(pNotificatonAudit.getComment());
      ehsAuditDto.setOverallStatus(pNotificatonAudit.getEhsStatus());
      ehsAuditDto.setVehicleRegistration(vehicle == null ? "NA" : vehicle.getRegistartionNumber());
      ehsAuditDto.setVendorName(vehicle == null ? "NA" : vehicle.getVendor());
      ehsAuditDto.setFieldsRejected(getFailedChecklists(pvehicleEHSChecklistAudits, checklistsMap));
      String cabTypeName = vehicle != null
          && vehicle.getVehicleType() != null ? vehicle.getVehicleType() : "NA";
      ehsAuditDto.setCabType(cabTypeName);
      ehsAuditDto.setDriver(ehsAdditionalLogDTO.getDriverName());
      ehsAuditDto.setKmsTravelled(ehsAdditionalLogDTO.getKmsTravelled());
      ehsAuditDto.setOffice(ehsAdditionalLogDTO.getFacility());
      ehsAuditDto.setEhsExpiryDate(pNotificatonAudit.getEhsExpiryTime());
      ehsAudits.add(ehsAuditDto);
    }
    return ehsAudits;
  }
  
  @Override
  public List<EHSAuditReportDTO> getEHSAuditReportFOrApproachingExpiry(String businessUnit, Date from, Date upto) {

    List<VehicleEHSNotificationStatusAuditDO> pNotificatonAudits = ehsNotAuditRepository
        .findEHSUpdatedByDateRangeExcept(businessUnit, from, upto, VehicleEHSStatus.PASSED.name(), true);
    List<EHSAuditReportDTO> ehsAudits = Lists.newArrayList();
    
    Map<String, Vehicle> vehiclesMap = vms.getVehiclesMap(businessUnit);
    Map<String, EHSChecklistDTO> checklistsMap = ehsChecklistService
        .getChecklistsById(Collections.<String> emptyList());

    for (VehicleEHSNotificationStatusAuditDO pNotificatonAudit : pNotificatonAudits) {
      String user = pNotificatonAudit.getRevinfo() != null
          && StringUtils.isNotBlank(pNotificatonAudit.getRevinfo().getMisUser()) ? pNotificatonAudit.getRevinfo()
          .getMisUser() : StringUtils.EMPTY;
      if (user.equals("System") && !pNotificatonAudit.getEhsStatus().equals(VehicleEHSStatus.EXPIRED.name())) {
        continue;
      }
      Vehicle vehicle = vehiclesMap.get(pNotificatonAudit.getEhsAuditId().getVehicleId());
      List<PVehicleEHSChecklistAudit> pvehicleEHSChecklistAudits = ehsAuditRepository.findEHSUpdatedVehicleForTimeStamp(
          businessUnit, pNotificatonAudit.getEhsAuditId().getVehicleId(),
          pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime());
      EHSAdditionalLogDTO ehsAdditionalLogDTO = ehLogService
          .getByUpdateTime(pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime());
      EHSAuditReportDTO ehsAuditDto = new EHSAuditReportDTO();
      ehsAuditDto.setAuditBy(user);
      ehsAuditDto.setChangeTime(sdf.format(pNotificatonAudit.getEhsAuditId().getEhsStatusUpdateTime()));
      ehsAuditDto.setComment(pNotificatonAudit.getComment());
      ehsAuditDto.setOverallStatus(pNotificatonAudit.getEhsStatus());
      ehsAuditDto.setVehicleRegistration(vehicle == null ? "NA" : vehicle.getRegistartionNumber());
      ehsAuditDto.setVendorName(vehicle == null ? "NA" : vehicle.getVendor());
      ehsAuditDto.setFieldsRejected(getFailedChecklists(pvehicleEHSChecklistAudits, checklistsMap));
      String cabTypeName = vehicle != null && vehicle.getVehicleType() != null ? vehicle.getVehicleType() : "NA";
      ehsAuditDto.setCabType(cabTypeName);
      ehsAuditDto.setDriver(ehsAdditionalLogDTO.getDriverName());
      ehsAuditDto.setKmsTravelled(ehsAdditionalLogDTO.getKmsTravelled());
      ehsAuditDto.setOffice(ehsAdditionalLogDTO.getFacility());
      ehsAuditDto.setEhsExpiryDate(pNotificatonAudit.getEhsExpiryTime());
      ehsAudits.add(ehsAuditDto);
    }
    return ehsAudits;
  }

  private List<String> getFailedChecklists(List<PVehicleEHSChecklistAudit> pEhsAudits,
      Map<String, EHSChecklistDTO> checklistsMap) {
    List<String> failedChecklists = Lists.newArrayList();
    for (PVehicleEHSChecklistAudit pAudit : pEhsAudits) {
      if (pAudit.getEhsStatus().equals(VehicleEHSStatus.FAILED.name()))
        failedChecklists.add(checklistsMap.get(pAudit.getVehicleEHSAuditId().getChecklistId()).getName());
    }
    return failedChecklists;
  }

  @Override
  public EHSAuditReportDTO processFailedEHSnotification(VehicleEHSChecklistVO ehsStatusResponseVO, String businessUnit,
      String email, String comment) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Vehicle vehicle = vms.getActiveVehicle(businessUnit, ehsStatusResponseVO.getRegistrationNumber());
    Map<String, EHSChecklistDTO> checklistIdNameMap = ehsChecklistService.getChecklistsById(Collections
        .<String> emptyList());
    List<String> fieldsRejected = Lists.newArrayList();
    String overallEhsStatus = null;
    Map<String, String> checklistStatusMap = ehsStatusResponseVO.getChecklistStatusMap();
    for (Map.Entry<String, String> entry : checklistStatusMap.entrySet()) {
      if (entry.getKey().equals(WebConstants.OVERALL_STATUS_ID)) {
        overallEhsStatus = entry.getValue();
        continue;
      } else if (entry.getValue().equals(VehicleEHSStatus.FAILED.name())) {
        fieldsRejected.add(checklistIdNameMap.get(entry.getKey()).getName());
      }
    }
    EHSAuditReportDTO ehsAuditReportDTO = new EHSAuditReportDTO();
    ehsAuditReportDTO.setChangeTime(sdf.format(new Date()));
    ehsAuditReportDTO.setVendorName(vehicle == null ? "NA" : vehicle.getVendor());
    ehsAuditReportDTO.setVehicleRegistration(vehicle == null ? "NA" : vehicle.getRegistartionNumber());
    ehsAuditReportDTO.setAuditBy(email);
    ehsAuditReportDTO.setOverallStatus(overallEhsStatus);
    ehsAuditReportDTO.setFieldsRejected(fieldsRejected);
    ehsAuditReportDTO.setComment(comment);
    return ehsAuditReportDTO;
  }

  @Override
  public void sendFailedEHSNotification(VehicleEHSChecklistVO ehsStatusResponseVO, EHSAuditReportDTO eDTO, String buId, String[] to, String[] replyTo) {
    List<Vehicle> vehicles = vms.getActiveVehicles(ehsStatusResponseVO.getBuid(), ehsStatusResponseVO.getRegistrationNumber());
    List<String> tempToList = Lists.newArrayList(to);
    if (!vehicles.isEmpty()) {
      for (Vehicle vehicle : vehicles) {
        if (ArrayUtils.isNotEmpty(vehicle.getVendorMailds())) {
          tempToList.addAll(Arrays.asList(vehicle.getVendorMailds()));
        }
      }
    }
    String[] toArray = new String[tempToList.size()];
    toArray = tempToList.toArray(toArray);
    String subject = "Vehicle No-Entry notification - " + eDTO.getVehicleRegistration() + " - " + eDTO.getVendorName();
    StringBuffer body = ComplianceUtils.prepareEmailBody(eDTO, buId);
    try {
      EmailTemplate template = new EmailTemplate(new Object[] { body },
          EmailTemplateConstants.EHS_INSTANT_FAILED_EMAIL_NOTIFICATION_TEMPLATE);
      communicationService.sendEmailUsingCommunicationChannnel(toArray, subject, template.getMailBody(),
          WebConstants.COMMUNICATION_CLIENT_ID);
      logger.debug("Email Sent for failed checklists !");
    } catch (Exception e) {
      logger.error("Mail Not Sent for failed checklists !" + e);
    }
  }
}
