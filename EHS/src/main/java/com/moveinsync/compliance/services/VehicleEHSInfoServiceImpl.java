package com.moveinsync.compliance.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.moveinsync.compliance.dto.BusinessUnitCheckListDTO;
import com.moveinsync.compliance.dto.EHSAdditionalLogDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.dto.VehicleEHSDTO;
import com.moveinsync.compliance.ets.model.EHSNotificationStatus;
import com.moveinsync.compliance.ets.persistence.entities.VehicleEHSNotificationStatusDO;
import com.moveinsync.compliance.ets.persistence.repository.VehicleEHSNotificationRepository;
import com.moveinsync.compliance.ets.services.service.VmsService;
import com.moveinsync.compliance.ets.web.util.WebConstants;
import com.moveinsync.compliance.models.Alerts;
import com.moveinsync.compliance.persistence.entities.PVehicleEHSChecklistStatus;
import com.moveinsync.compliance.persistence.repository.VehicleEHSRepository;
import com.moveinsync.compliance.types.VehicleEHSStatus;

/**
 * 
 * @author rajanish
 * @since 18 March 2018
 *
 */

@Service
public class VehicleEHSInfoServiceImpl implements VehicleEHSService {

	@Autowired
	VehicleEHSRepository vrepo;

	@Autowired
	VmsService vms;

	@Autowired
	BusinessUnitCheckListService buCheckService;

	@Autowired
	EHSChecklistService ehsChecklistService;

	@Autowired
	VehicleEHSNotificationRepository vehicleEHSNotificationRepository;

	@Autowired
	EHSAdditionalLogService ehsLogService;

	@Autowired
	ComplianceV2ServiceImpl compV2;

	private final Logger logger = LoggerFactory.getLogger(VehicleEHSInfoServiceImpl.class);

	@Override
	public List<VehicleEHSDTO> getEHSByVehicleId(String buId, String vehicleId) {
		List<VehicleEHSDTO> vehicleEHSInfoDTOs = Lists.newArrayList();
		String vehicleComplianceStatus = compV2.getVehicleComplianceStatus(buId, vehicleId);
		List<PVehicleEHSChecklistStatus> pVehicleEHSInfos = vrepo.findByVehicleId(vehicleId);
		if (pVehicleEHSInfos.isEmpty()) {
			return vehicleEHSInfoDTOs;
		}
		for (PVehicleEHSChecklistStatus pInfo : pVehicleEHSInfos) {
			vehicleEHSInfoDTOs.add(convert(pInfo, vehicleComplianceStatus));
		}
		return vehicleEHSInfoDTOs;
	}

	@Override
	public void save(VehicleEHSChecklistVO eVO, String businessUnit) {
		List<VehicleEHSDTO> vehicleEHSInfoDTOs = Lists.newArrayList();
		List<EHSAdditionalLogDTO> ehsAdditionalLogDTOs = Lists.newArrayList();
		Date updateTime = new Date();
		EHSAdditionalLogDTO eDto = convert(eVO, updateTime);
		ehsAdditionalLogDTOs.add(eDto);
		ehsLogService.save(ehsAdditionalLogDTOs);
		Map<String, String> checklistStatusMap = eVO.getChecklistStatusMap();
		/* Store Overall status with comment in vehicle_ehs_notification relation */
		for (Map.Entry<String, String> entry : checklistStatusMap.entrySet()) {
			if (entry.getKey().equals(WebConstants.OVERALL_STATUS_ID)) {
				VehicleEHSNotificationStatusDO ehsNotificationStatusDO = new VehicleEHSNotificationStatusDO(
						eVO.getVehicleId(), businessUnit);
				ehsNotificationStatusDO.setComment(eVO.getComment());
				ehsNotificationStatusDO.setEhsExpiryTime(getEHSExpiryDate(businessUnit));
				ehsNotificationStatusDO.setEhsStatus(entry.getValue());
				ehsNotificationStatusDO.setEhsStatusUpdateTime(updateTime);
				ehsNotificationStatusDO.setNotificationStatus(EHSNotificationStatus.NOT_PROCESSED.name());
				ehsNotificationStatusDO.setEhsAudited(true);
				vehicleEHSNotificationRepository.save(ehsNotificationStatusDO);
				logger.debug(
						"Vehicle EHS Checklist overall status updatedTime: {} of businessUnitId {} and Vehicle Id {} and ",
						updateTime, businessUnit, eVO.getVehicleId());
				;
				// Update EHS status for compliance page
				compV2.updateEhsStatus(businessUnit, eVO.getVehicleId(), eVO.getRegistrationNumber(), entry.getValue());
			} else {
				/* Store rest of checklist info in vehicle_ehs_checklist relation */
				VehicleEHSDTO vDto = new VehicleEHSDTO();
				vDto.setBuid(eVO.getBuid());
				vDto.setChecklistId(entry.getKey());
				vDto.setEhsStatus(VehicleEHSStatus.valueOf(entry.getValue()));
				vDto.setEhsStatusUpdateTime(updateTime);
				vDto.setEhsExpiryTime(getEHSExpiryDate(businessUnit));
				vDto.setNotificationStatus(EHSNotificationStatus.NOT_PROCESSED);
				vDto.setVehicleId(eVO.getVehicleId());
				vDto.setComment(eVO.getComment());
				vehicleEHSInfoDTOs.add(vDto);
			}
		}
		persist(vehicleEHSInfoDTOs);
	}

	private void persist(List<VehicleEHSDTO> vehicleEHSInfoDTOs) {
		List<PVehicleEHSChecklistStatus> pEhsInfos = Lists.newArrayList();
		for (VehicleEHSDTO vDto : vehicleEHSInfoDTOs) {
			pEhsInfos.add(convert(vDto));
		}
		vrepo.saveAll(pEhsInfos);
	}

	private Date getEHSExpiryDate(String businessUnit) {
		Alerts alerts = compV2.getVehicleAlerts(businessUnit);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, alerts.getEhsStatusExpiryHours());
		return cal.getTime();
	}

	private PVehicleEHSChecklistStatus convert(VehicleEHSDTO vDto) {
		PVehicleEHSChecklistStatus pInfo = new PVehicleEHSChecklistStatus(vDto.getVehicleId(), vDto.getBuid(),
				vDto.getChecklistId());
		pInfo.setEhsExpiryTime(vDto.getEhsExpiryTime());
		pInfo.setEhsStatus(vDto.getEhsStatus() != null ? vDto.getEhsStatus().name() : null);
		pInfo.setEhsStatusUpdateTime(vDto.getEhsStatusUpdateTime());
		pInfo.setNotificationStatus(vDto.getNotificationStatus().name());
		pInfo.setComment(vDto.getComment());
		return pInfo;
	}

	private EHSAdditionalLogDTO convert(VehicleEHSChecklistVO vVo, Date updateTime) {
		EHSAdditionalLogDTO eDto = new EHSAdditionalLogDTO();
		eDto.setBusinessUnit(vVo.getBuid());
		eDto.setDriverName(vVo.getDriverName());
		eDto.setFacility(vVo.getFacility());
		eDto.setKmsTravelled(vVo.getKmsTravelled());
		eDto.setVehicleId(vVo.getVehicleId());
		eDto.setUpdateTime(updateTime);
		return eDto;
	}

	@Override
	public void generateDefaultEntries(String businessUnit, List<BusinessUnitCheckListDTO> buCheckListDTOs) {
		List<PVehicleEHSChecklistStatus> pVehicleEHSInfos = Lists.newArrayList();
		List<String> vehicleIds = compV2.getVehicleIdsForBu(businessUnit);
		for (String vehiceId : vehicleIds) {

			for (BusinessUnitCheckListDTO buCheckListDTO : buCheckListDTOs) {
				PVehicleEHSChecklistStatus pVehicleEHSInfo = new PVehicleEHSChecklistStatus(vehiceId, businessUnit,
						buCheckListDTO.getChecklistId());
				pVehicleEHSInfo.setComment(null);
				pVehicleEHSInfo.setEhsExpiryTime(getEHSExpiryDate(businessUnit));
				pVehicleEHSInfo.setEhsStatus(VehicleEHSStatus.FAILED.name());
				pVehicleEHSInfo.setEhsStatusUpdateTime(new Date());
				pVehicleEHSInfo.setNotificationStatus(EHSNotificationStatus.NOT_PROCESSED.name());
				pVehicleEHSInfos.add(pVehicleEHSInfo);
			}
		}
		vrepo.saveAll(pVehicleEHSInfos);
	}

	private VehicleEHSDTO convert(PVehicleEHSChecklistStatus pInfo, String compStatus) {
		VehicleEHSDTO vDto = new VehicleEHSDTO();
		vDto.setBuid(pInfo.getId().getBusinessUnitId());
		vDto.setChecklistId(pInfo.getId().getChecklistId());
		vDto.setComplianceStatus(compStatus);
		vDto.setVehicleId(pInfo.getId().getVehicleId());
		vDto.setEhsExpiryTime(pInfo.getEhsExpiryTime());
		vDto.setEhsStatus(VehicleEHSStatus.valueOf(pInfo.getEhsStatus()));
		vDto.setEhsStatusUpdateTime(pInfo.getEhsStatusUpdateTime());
		vDto.setNotificationStatus(EHSNotificationStatus.valueOf(pInfo.getNotificationStatus()));
		vDto.setComment(pInfo.getComment());
		return vDto;
	}
}
