package com.moveinsync.compliance.services;

import java.util.List;

import com.moveinsync.compliance.dto.BusinessUnitCheckListDTO;
import com.moveinsync.compliance.dto.VehicleEHSChecklistVO;
import com.moveinsync.compliance.dto.VehicleEHSDTO;

public interface VehicleEHSService {

	public void save(VehicleEHSChecklistVO ehsStatusResponseVO, String businessUnit);

	public List<VehicleEHSDTO> getEHSByVehicleId(String buId, String vehicleId);

	public void generateDefaultEntries(String businessUnit, List<BusinessUnitCheckListDTO> businessUnitCheckListDTOs);

}
