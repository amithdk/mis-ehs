package com.moveinsync.compliance.types;

public enum VehicleEHSStatus {
	PASSED, EXPIRED, FAILED, APPROACHING_EXPIRY
}
