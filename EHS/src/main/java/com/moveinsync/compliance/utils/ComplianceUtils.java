package com.moveinsync.compliance.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.moveinsync.compliance.dto.EHSAuditReportDTO;
import com.moveinsync.data.envers.utils.AuditProperties;

@Component
public class ComplianceUtils {

	public static StringBuffer prepareEmailBody(EHSAuditReportDTO eDTO, String buId) {
		StringBuffer sb = new StringBuffer();
		sb.append("<table border='1'>");
		sb.append(getHeading());
		sb.append("<tbody>");
		sb.append("<tr><td>");
		sb.append(eDTO.getChangeTime());
		sb.append("</td><td>");
		sb.append(eDTO.getVendorName());
		sb.append("</td><td>");
		sb.append(eDTO.getVehicleRegistration());
		sb.append("</td><td>");
		sb.append(eDTO.getAuditBy());
		sb.append("</td><td>");
		sb.append(eDTO.getOverallStatus());
		sb.append("</td><td>");
		sb.append(eDTO.getFieldsRejected());
		sb.append("</td><td>");
		sb.append(eDTO.getComment());
		sb.append("</td></tr>");
		sb.append("</tbody></table>");
		return sb;
	}

	private static String getHeading() {
		StringBuffer sb = new StringBuffer();
		sb.append("<thead style='background-color: #948D8D'>");
		sb.append("<tr>");
		sb.append("<th>Change Time</th>");
		sb.append("<th>Vendor</th>");
		sb.append("<th>Registration</th>");
		sb.append("<th>Audited By</th>");
		sb.append("<th>Status</th>");
		sb.append("<th>Fields Rejected</th>");
		sb.append("<th>Comments</th>");
		sb.append("</tr></thead>");
		return sb.toString();
	}

	public static void setAuditDetails(String userName, String comment) {
		AuditProperties.setUser(userName);
	}

	public static String getUrlFromBU(String Buid) {
		String[] siteName = StringUtils.split(Buid, '-');
		String url = "https://" + siteName[0] + ".moveinsync.com/" + siteName[1];
		return url;
	}
}
